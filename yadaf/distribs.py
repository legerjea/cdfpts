# Copyright 2018-2020, Université de Technologie de Compiègne, France,
#                      Jean-Benoist Leger <jbleger@hds.utc.fr>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from typing import Tuple, Union, Callable, Any, Iterable
import numpy as np
import scipy as sp
import scipy.stats
import scipy.optimize
from .cpd import ContinuousProbabilityDistribution


def from_cdf(fcdf: Callable[[float], float], bounds: Tuple[float, float] = None):
    """Define a ContinuousProbabilityDistribution from a cdf.
    
    This function is provided for convenience. This way of generating a
    ContinuousProbabilityDistribution is very slow. Use the ppf directly if you
    can.
    
    Parameters
    ----------
     - fcdf : function
       The cumulative function distribution used to build the distrib.
     - bounds : tuple of 2 values or None
       If provided, the bounds of the support of the distribution. Bounds are 
       deduced from the cdf if None."""

    if not hasattr(fcdf, "__call__"):
        raise TypeError("fcdf must be a callable")
    if bounds is None:
        b1, b2 = -1, 1
        while fcdf(b1) > 1e-12 and b1 > -1e200:
            b1 *= 2
        while 1 - fcdf(b2) > 1e-12 and b2 < 1e200:
            b2 *= 2
        if b2 - b1 >= 1e200:
            raise TypeError("The fcdf must be a cumulative distrib function")
    else:
        b1, b2 = bounds

    scalar_ppf = lambda y: sp.optimize.bisect(lambda x: fcdf(x) - y, b1, b2)
    return ContinuousProbabilityDistribution(
        ppf=lambda X: np.array([scalar_ppf(x) for x in X])
    )


def mixture(
    components: Iterable[Tuple[float, Any]], bounds: Tuple[float, float] = None
):
    """Define a ContinuousProbabilityDistribution with a mixture of
    distribution.

    Parameters
    ----------
     - components: iterable of 2 elements tuples. 
       Describes the components. For each tuple, the first component is the
       weight of the component, the second a object which must have a cdf
       method.
       Weight must sum to one.
       Example, for a mixture of 2 gaussian using scipy:
        - components=[(0.2, sp.stats.norm(0,2)), (0.8, sp.stats.norm(4,1))]
       Example, for a mixture of 2 distributions described by
       ContinuousProbabilityDistribution objects A and B:
        - components=[(0.2, A), (0.8, B)]
     - bounds : tuple of 2 values or None
       If provided, the bounds of the support of the distribution. Bounds are 
       If provided, the bounds of the support of the distribution. Bounds are 
       deduced if None."""

    comps = list(components)

    if not all(
        (
            hasattr(x, "__len__")
            and len(x) == 2
            and isinstance(x[0], float)
            and hasattr(x[1], "cdf")
            and hasattr(x[1].cdf, "__call__")
        )
        for x in comps
    ):
        raise TypeError(
            "components: iterable of 2 elements tuples, (float,  object with cdf method)"
        )
    sum_weight = sum(w for w, c in components)
    if not np.isclose(sum_weight, 1):
        raise ValueError("Weight must sum to one")
    cdf = lambda x: sum(w * c.cdf(x) for w, c in comps) / sum_weight
    return from_cdf(cdf, bounds)
