# Copyright 2018-2020, Université de Technologie de Compiègne, France,
#                      Jean-Benoist Leger <jbleger@hds.utc.fr>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import numpy as np

from . import _stuff
from ._internal import basic
from ._lazy import lazy_cached_method_getter

logistic = lambda x: 1 / (1 + np.exp(-x))
logit = lambda p: np.log(p) - np.log(1-p)


class PointLenError(Exception):
    pass


class InitializationError(Exception):
    pass


class ContinuousProbabilityDistribution:
    """Class representing a random variate.

    Random variate is represented by a finite set of values from cdf.
    Values used are in equally separated in the logistic scale to have a good
    representation of tails.

    Approximation of cdf, pdf, ppf, can be retrieved.

    Monotonous functions can be applied. Sum between random variates can be
    applied."""

    def __init__(
        self,
        dist=None,
        *,
        ppf=None,
        cdf=None,
        sf=None,
        logitcdf=None,
        nbpts=401,
        bounds=20,
        xpts_values=None,
        lypts_values=None,
        scale="xylinear",
    ):
        """Initializer of random variate.

        A random variate can be initialized with a ppf function (should be
        used), or with values representing the cdf (should not be used unless
        you know exactly what are you doing)."""

        self._scale = scale
        if dist is not None and (ppf is not None or cdf is not None or sf is not None or logitcdf is not None):
            raise InitializationError('when dist is provided, ppf, cdf, sf, logitcdf can not be provided')
        if dist is not None:
            if not hasattr(dist, 'ppf') or not hasattr(dist, 'cdf') or not hasattr(dist, 'sf'):
                raise TypeError('dist must have ppf, cdf and sf methods')
            ppf = dist.ppf
            logitcdf = lambda x: np.log(dist.cdf(x))-np.log(dist.sf(x))

        if xpts_values is not None and lypts_values is not None:
            if len(xpts_values) != len(lypts_values):
                raise PointLenError
            self._xpts = xpts_values.copy()
            self._lypts = lypts_values.copy()
            return None

        if ppf is None:
            raise InitializationError("if dist is not provided, ppf must be provided")
        if logitcdf is not None:
            if cdf is not None or sf is not None:
                raise InitializationError("when logitcdf is provided, cdf and sf are not used")
        else:
            if cdf is None or sf is None:
                raise InitializationError("when dist is not provided, logitcdf xor (cdf and sf) must be provided")
            logitcdf = lambda x: np.log(cdf(x))-np.log(sf(x))

        if ppf is not None:
            if scale == "logit":
                self._lypts = np.linspace(-bounds, bounds, nbpts)
                self._xpts = ppf(logistic(self._lypts))
            elif scale == "linear":
                self._lypts = logit(
                    np.linspace(logistic(-bounds), logistic(bounds), nbpts)
                )
                self._xpts = ppf(logistic(self._lypts))
            elif scale == "linlogitmixture":
                self._lypts = 0.5 * (
                    np.linspace(-bounds, bounds, nbpts)
                    + logit(np.linspace(logistic(-bounds), logistic(bounds), nbpts))
                )
                self._xpts = ppf(logistic(self._lypts))
            elif scale == "xylinear":
                y1 = np.linspace(logistic(-bounds), logistic(bounds), nbpts)
                x1 = ppf(y1)
                x2 = np.linspace(x1[0], x1[-1], nbpts)
                x12 = np.concatenate((x1,x2))
                x12.sort(kind='mergesort')
                for _ in range(8):
                    x12[1:-1] = (x12[:-2]+x12[2:])/2
                x3 = (x12[0::2] + x12[1::2])/2
                self._xpts = x3
                self._lypts = logitcdf(x3)

            else:
                raise InitializationError
            return None

        raise InitializationError

    def ppf(self, x):
        """ppf of the random variate"""

        if hasattr(x, "__array__"):
            if np.any(x <= 0) or np.any(x >= 1):
                raise ValueError
            return np.asarray(basic.ppf_vector(self._xpts, self._lypts, x))
        if x <= 0 or x >= 1:
            raise ValueError
        return basic.ppf_scalar(self._xpts, self._lypts, x)

    def cdf(self, x):
        """cdf of the random variate"""

        if hasattr(x, "__array__"):
            return np.asarray(basic.cdf_vector(self._xpts, self._lypts, x))
        return basic.cdf_scalar(self._xpts, self._lypts, x)

    def logcdf(self, x):
        """logcdf of the random variate"""

        if hasattr(x, "__array__"):
            return np.asarray(basic.logcdf_vector(self._xpts, self._lypts, x))
        return basic.logcdf_scalar(self._xpts, self._lypts, x)

    def sf(self, x):
        """sf of the random variate"""

        if hasattr(x, "__array__"):
            return np.asarray(basic.sf_vector(self._xpts, self._lypts, x))
        return basic.sf_scalar(self._xpts, self._lypts, x)

    def logsf(self, x):
        """logsf of the random variate"""

        if hasattr(x, "__array__"):
            return np.asarray(basic.logsf_vector(self._xpts, self._lypts, x))
        return basic.logsf_scalar(self._xpts, self._lypts, x)

    def logitcdf(self, x):
        """logitcdf of the random variate"""

        if hasattr(x, "__array__"):
            return np.asarray(basic.logitcdf_vector(self._xpts, self._lypts, x))
        return basic.logitcdf_scalar(self._xpts, self._lypts, x)

    def pdf(self, x):
        """pdf of the random variate"""

        if hasattr(x, "__array__"):
            return np.asarray(basic.pdf_vector(self._xpts, self._lypts, x))
        return basic.pdf_scalar(self._xpts, self._lypts, x)

    def logpdf(self, x):
        """logpdf of the random variate"""

        if hasattr(x, "__array__"):
            return np.asarray(basic.logpdf_vector(self._xpts, self._lypts, x))
        return basic.logpdf_scalar(self._xpts, self._lypts, x)

    def dpdf(self, x):
        """Derivative of the pdf of the random variate"""

        if hasattr(x, "__array__"):
            return np.asarray(basic.dpdf_vector(self._xpts, self._lypts, x))
        return basic.dpdf_scalar(self._xpts, self._lypts, x)

    def expectation_monomial(self, k, a=0):
        """Compute `E((X-a)**k)` where X is the random variate"""

        if k <= 0:
            raise ValueError("exponent must be positive")
        if not isinstance(k, int):
            raise TypeError("exponent must a integer")
        return basic.expectation(self._xpts, self._lypts, k, a)

    def moment(self, order, centered=False):
        """Returns the `order`-th moment of the random variate"""

        if centered:
            a = self.mean()
        else:
            a = 0
        return self.expectation_monomial(order, a)

    @lazy_cached_method_getter
    def mean(self):
        """
        Compute the mean of the random variate.
        """
        return self.moment(1)

    @lazy_cached_method_getter
    def median(self):
        """
        Compute the mean of the random variate.
        """
        return self.ppf(0.5)

    @lazy_cached_method_getter
    def var(self):
        """
        Compute the variance of the random variate.
        """
        return self.moment(2, centered=True)

    @lazy_cached_method_getter
    def std(self):
        """
        Compute the standard deviation of the random variate.
        """
        return np.sqrt(self.var())

    @lazy_cached_method_getter
    def skewness(self):
        """
        Compute the skeness of the random variate.
        """
        return self.moment(3, centered=True) / self.std() ** 3

    @lazy_cached_method_getter
    def kurtosis(self):
        """
        Compute the kurtosis of the random variate.

        See excess_kurtosis.
        """
        return self.moment(4, centered=True) / self.var() ** 2

    @lazy_cached_method_getter
    def excess_kurtosis(self):
        """
        Compute the excess kurtosis of the random variate.

        See kurtosis.
        """
        return self.kurtosis() - 3

    def entropy(self):
        """Compute `-E(log p(X))=-∫p(x)log(p(x))dx` where X is the random variate and p the pdf."""

        return basic.entropy(self._xpts, self._lypts)

    def confint(self, confidence: float):
        """
        Compute the minimum length confidence intervals.

        Parameter
        ---------
         confidence : float
           Confidence level. Must be in ]0,1[.

        Returns
        -------
        list of 2-elements tuple
           Each tuple represents a interval. The union of all intervals is the
           minimum set with provided confidence level.
        """
        if not isinstance(confidence, float):
            raise TypeError("confidence must be a float")
        if not (0 < confidence < 1):
            raise ValueError("confidence must be in ]0,1[")
        res = np.asarray(basic.confint(self._xpts, self._lypts, confidence))
        return [(x, y) for (x, y) in res.T]

    def __add__(self, oth):
        if isinstance(oth, ContinuousProbabilityDistribution):
            return _stuff.add(self, oth)
        if isinstance(oth, float) or isinstance(oth, int):
            return self.apply_monotonic_function(lambda x: x + oth)
        raise TypeError

    def __sub__(self, oth):
        if isinstance(oth, ContinuousProbabilityDistribution):
            return _stuff.add(self, oth.apply_monotonic_function(lambda x: -x))
        if isinstance(oth, float) or isinstance(oth, int):
            return self.apply_monotonic_function(lambda x: x - oth)
        raise TypeError

    def __mul__(self, oth):
        if isinstance(oth, float) or isinstance(oth, int):
            return self.apply_monotonic_function(lambda x: x * oth)
        raise TypeError

    def __truediv__(self, oth):
        if isinstance(oth, float) or isinstance(oth, int):
            return self.apply_monotonic_function(lambda x: x / oth)
        raise TypeError

    def __radd__(self, oth):
        return self.__add__(oth)

    def __rsub__(self, oth):
        if isinstance(oth, ContinuousProbabilityDistribution):
            return _stuff.add(self.apply_monotonic_function(lambda x: -x), oth)
        if isinstance(oth, float) or isinstance(oth, int):
            return self.apply_monotonic_function(lambda x: oth - x)
        raise TypeError

    def __rmul__(self, oth):
        return self.__mul__(oth)

    def __rtruediv__(self, oth):
        if isinstance(oth, float) or isinstance(oth, int):
            return self.apply_monotonic_function(lambda x: oth / x)
        raise TypeError

    def apply_monotonic_function(self, fun):
        xfunpts = fun(self._xpts)
        dxfunpts = np.diff(xfunpts)
        if np.all(dxfunpts > 0):
            lyfunpts = self._lypts
        elif np.all(dxfunpts < 0):
            xfunpts = xfunpts[::-1]
            lyfunpts = -self._lypts[::-1]
        else:
            raise ValueError("Function must be strictly monotonic")
        return ContinuousProbabilityDistribution(
            xpts_values=xfunpts, lypts_values=lyfunpts, scale=self._scale
        )
