# Copyright 2018-2020, Université de Technologie de Compiègne, France,
#                      Jean-Benoist Leger <jbleger@hds.utc.fr>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from . import cpd
from ._internal import intprod
from ._internal import basic

import numpy as np


def _extend(xpts, lypts):
    return np.concatenate(
        (
            (xpts[0] - 5 * (xpts[1] - xpts[0]) / (lypts[1] - lypts[0]),),
            xpts,
            (xpts[-1] + 5 * (xpts[-1] - xpts[-2]) / (lypts[-1] - lypts[-2]),),
        )
    )


def add(X, Y):
    if not isinstance(X, cpd.ContinuousProbabilityDistribution):
        raise TypeError
    if not isinstance(Y, cpd.ContinuousProbabilityDistribution):
        raise TypeError
    npts = max(len(X._xpts), len(Y._xpts))

    ts = (
        _extend(X._xpts, X._lypts).reshape(1, -1)
        + _extend(Y._xpts, Y._lypts).reshape((-1, 1))
    ).flatten()
    ts.sort(kind="mergesort")
    evt = np.asarray(basic.subsample(ts, npts + 2, .5))
    evpdf = intprod.intprod_reverseB_multiple(X._xpts, X._lypts, Y._xpts, Y._lypts, evt)
    evcdf = np.asarray(intprod.integrate_pdf(evt, evpdf))
    return _build_cpd_with_evxpy(
        evt,
        evcdf,
        scale=X._scale if X._scale == Y._scale else "xylinear",
        nbpts=max(len(X._xpts), len(Y._xpts)),
    )


def _build_cpd_with_evxpy(evxpts, evpypts, scale, nbpts):
    evxpts = np.asarray(evxpts)
    evpypts = np.asarray(evpypts)
    newx = evxpts[1:-1]
    with np.errstate(under="ignore"):
        newcdf = (evpypts[1:-1] - evpypts[0]) / (evpypts[-1] - evpypts[0])
    mask = (newcdf > 0) & (newcdf < 1)
    newx = newx[mask]
    newcdf = newcdf[mask]
    logitnewcdf = cpd.logit(newcdf)
    conserved = [0]
    for i in range(len(newx)):
        if newx[i] - newx[conserved[-1]] > 1e-10:
            if logitnewcdf[i] > logitnewcdf[conserved[-1]]:
                conserved.append(i)
    newx = newx[conserved]
    logitnewcdf = logitnewcdf[conserved]
    ret = cpd.ContinuousProbabilityDistribution(
        ppf=lambda x: np.asarray(basic.ppf_vector(newx, logitnewcdf, x)),
        logitcdf=lambda x: np.asarray(basic.logitcdf_vector(newx, logitnewcdf, x)),
        nbpts=nbpts,
        scale=scale,
    )
    return ret


def cond_lin(X, Y):
    evxpts, evpypts = intprod.intprod_direct(X._xpts, X._lypts, Y._xpts, Y._lypts)
    ret = _build_cpd_with_evxpy(
        evxpts,
        evpypts,
        scale=X._scale if X._scale == Y._scale else "xylinear",
        nbpts=max(len(X._xpts), len(Y._xpts)),
    )
    return ret


def cond_likli(X, likli):
    evxpts, evpypts = intprod.intprod_direct_likli(X._xpts, X._lypts, likli, 2.0)
    ret = _build_cpd_with_evxpy(evxpts, evpypts, scale=X._scale, nbpts=len(X._xpts))
    return ret


def cond_likli_gm(X, gm_K, gm_c, gm_prec):
    evxpts, evpypts = intprod.intprod_direct_likli_gm(
        X._xpts, X._lypts, gm_K, gm_c, gm_prec, 2.0
    )
    ret = _build_cpd_with_evxpy(evxpts, evpypts, scale=X._scale, nbpts=len(X._xpts))
    return ret
