# Copyright 2018, Université de Technologie de Compiègne, France,
#                 Jean-Benoist Leger <jbleger@hds.utc.fr>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import pytest
from .._internal import basic
import scipy as sp
import scipy.stats
import numpy as np
from ..cpd import logit, logistic


def test_ppf_one():
    lypts = np.linspace(-20, 20, 41)
    xpts = sp.stats.norm(1, 2).ppf(logistic(lypts))
    assert np.abs(basic.ppf_scalar(xpts, lypts, 0.5) - 1) < 1e-10


def test_ppf_vector():
    lypts = np.linspace(-20, 20, 41)
    xpts = sp.stats.norm(1, 2).ppf(logistic(lypts))
    p = 0.5 * np.ones(3)
    assert (
        np.max(np.abs(np.asarray(basic.ppf_vector(xpts, lypts, p)) - np.ones(3)))
        < 1e-10
    )
