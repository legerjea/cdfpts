# Copyright 2018-2020, Université de Technologie de Compiègne, France,
#                      Jean-Benoist Leger <jbleger@hds.utc.fr>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import pytest
import itertools
from .. import ContinuousProbabilityDistribution
from .. import hellinger_distance
from .. import bayes_lin, bayes_likli, bayes_likli_gm
import numpy as np
import scipy as sp
import scipy.stats

hellinger_allowed_error = 1e-3
hellinger_allowed_error2 = 5e-3  # difficult case
mean_allowed_error = 5e-4
std_allowed_error = 5e-4
mean_allowed_error2 = 2e-3  # difficult
std_allowed_error2 = 2e-3  # difficult

values_to_check_norm = [
    (prior_m, prior_s, noise_s, observed)
    for prior_m in np.linspace(-3, 3, 11)
    for prior_s in (1, 2, 4, 8)
    for noise_s in (1, 2, 4, 8)
    for observed in np.linspace(-4, 4, 9)
]


@pytest.mark.parametrize("p", values_to_check_norm)
def test_bayes_lin_prior_normal(p):
    prior_m, prior_s, noise_s, observed = p
    prior = ContinuousProbabilityDistribution(sp.stats.norm(prior_m, prior_s))
    noise = ContinuousProbabilityDistribution(sp.stats.norm(0, noise_s))

    posterior = bayes_lin(prior, noise, observed)

    # theo computation for normal dist
    theo_posterior_m = (prior_m / prior_s ** 2 + observed / noise_s ** 2) / (
        1 / prior_s ** 2 + 1 / noise_s ** 2
    )
    theo_posterior_s = np.sqrt(1 / (1 / prior_s ** 2 + 1 / noise_s ** 2))
    theo_posterior = ContinuousProbabilityDistribution(
        sp.stats.norm(theo_posterior_m, theo_posterior_s)
    )
    assert np.abs(posterior.mean() - theo_posterior_m) < mean_allowed_error
    assert np.abs(posterior.std() - theo_posterior_s) < std_allowed_error
    assert hellinger_distance(posterior, theo_posterior) < hellinger_allowed_error


values_to_check_truncate_norm = [
    (prior_m, prior_s, noise_e, observed)
    for prior_m in np.linspace(-3, 3, 11)
    for prior_s in (1, 2, 4, 8)
    for noise_e in (1, 2, 4, 8)
    for observed in np.linspace(-np.pi, np.pi, 9)
]


@pytest.mark.parametrize("p", values_to_check_truncate_norm)
def test_bayes_lin_prior_truncate_normal(p):
    prior_m, prior_s, noise_e, observed = p
    prior = ContinuousProbabilityDistribution(sp.stats.norm(prior_m, prior_s),nbpts=1001)
    noise = ContinuousProbabilityDistribution(
        sp.stats.uniform(-noise_e, 2 * noise_e)
    )

    posterior = bayes_lin(prior, noise, observed)

    # theo computation for normal dist and uniform
    # should be normal(prior_m, prior_s**2) truncated to
    # [observed-noise_e,observed+noise_e]
    A = sp.stats.norm(prior_m, prior_s).cdf(observed - noise_e)
    B = sp.stats.norm(prior_m, prior_s).cdf(observed + noise_e)
    dbase = sp.stats.norm(prior_m, prior_s)
    theo_cdf = lambda x: (dbase.cdf(x)-A)/(B-A)*(np.abs(x-observed)<noise_e)+(x-observed>=noise_e)

    s = np.linspace(observed - 2 * noise_e, observed + 2 * noise_e, 100000)
    assert np.max(np.abs(posterior.cdf(s) - theo_cdf(s))) < 3e-2


values_to_check_bayes_likli = [
    (prior_m, prior_s, likli_c, likli_s, scale)
    for prior_m in np.linspace(-1, 1, 7)
    for prior_s in (1.5, 2, 4)
    for likli_c in (0.5, 1, 2)
    for likli_s in (1.5, 2, 4)
    for scale in ("logit", "linear")
]


@pytest.mark.parametrize("p", values_to_check_bayes_likli)
def test_bayes_likli_fun_normal(p):
    (prior_m, prior_s, likli_c, likli_s, scale) = p
    prior = ContinuousProbabilityDistribution(
        sp.stats.norm(prior_m, prior_s), scale=scale, nbpts=1001
    )
    likli = lambda x: np.exp(-0.5 * ((x - likli_c) / likli_s) ** 2)
    dlikli = (
        lambda x: -(x - likli_c)
        / likli_s ** 2
        * np.exp(-0.5 * ((x - likli_c) / likli_s) ** 2)
    )

    posterior = bayes_likli(prior, (likli, dlikli))

    theo_posterior_m = (prior_m / prior_s ** 2 + likli_c / likli_s ** 2) / (
        1 / prior_s ** 2 + 1 / likli_s ** 2
    )
    theo_posterior_s = np.sqrt(1 / (1 / prior_s ** 2 + 1 / likli_s ** 2))
    theo_posterior = ContinuousProbabilityDistribution(
        sp.stats.norm(theo_posterior_m, theo_posterior_s), scale=scale
    )

    assert np.abs(posterior.mean() - theo_posterior_m) < mean_allowed_error2
    assert np.abs(posterior.std() - theo_posterior_s) < std_allowed_error2
    assert hellinger_distance(posterior, theo_posterior) < hellinger_allowed_error2
    assert posterior._scale == scale


values_to_check_truncate_norm_lik = [
    (prior_m, prior_s, noise_e, observed)
    for prior_m in np.linspace(-3, 3, 11)
    for prior_s in (4, 8, 16, 32)
    for noise_e in (1, 2, 3)
    for observed in np.linspace(-np.pi, np.pi, 9)
]


@pytest.mark.parametrize("p", values_to_check_truncate_norm_lik)
def test_bayes_likli_fun_prior_truncate_normal(p):
    prior_m, prior_s, noise_e, observed = p
    prior = ContinuousProbabilityDistribution(
        sp.stats.norm(prior_m, prior_s), scale="linear", nbpts=1001
    )

    posterior = bayes_likli(
        prior, (lambda x: (1 if np.abs(x - observed) < noise_e else 0), lambda x: 0)
    )

    # theo computation for normal dist and uniform
    # should be normal(prior_m, prior_s**2) truncated to
    # [observed-noise_e,observed+noise_e]
    A = sp.stats.norm(prior_m, prior_s).cdf(observed - noise_e)
    B = sp.stats.norm(prior_m, prior_s).cdf(observed + noise_e)
    dbase = sp.stats.norm(prior_m, prior_s)
    theo_cdf = lambda x: (dbase.cdf(x)-A)/(B-A)*(np.abs(x-observed)<noise_e)+(x-observed>=noise_e)

    s = np.linspace(observed - 2 * noise_e, observed + 2 * noise_e, 100000)
    assert np.max(np.abs(posterior.cdf(s) - theo_cdf(s))) < 3e-2


values_to_check_bayes_likli_gm = [
    (comps, scale)
    for comps in itertools.permutations(
        [(K, c, p) for K in (1, 2) for c in (-0.2, 0.5) for p in (1, 3)], 3
    )
    for scale in ("linear", "logit")
]


@pytest.mark.parametrize("p", values_to_check_bayes_likli_gm)
def test_bayes_likli_gm(p):
    comps, scale = p
    # prior
    prior = ContinuousProbabilityDistribution(
        sp.stats.norm(0, 1), scale=scale, nbpts=1001
    )

    def likfun_one(comp, x):
        L = comp[0] * np.exp(-0.5 * (x - comp[1]) ** 2 * comp[2])
        dL = -(x - comp[1]) * comp[2] * L
        return np.array([L, dL])

    lik_dlik_fin = lambda x: sum(likfun_one(comp, x) for comp in comps)

    posterior_m1 = bayes_likli(prior, lik_dlik_fin)
    posterior_m2 = bayes_likli_gm(prior, comps)
    assert np.abs(posterior_m1.mean() - posterior_m2.mean()) < 1e-10
    assert np.abs(posterior_m1.std() - posterior_m2.std()) < 1e-10
    assert hellinger_distance(posterior_m1, posterior_m2) < hellinger_allowed_error
    assert posterior_m1._scale == scale
    assert posterior_m2._scale == scale
