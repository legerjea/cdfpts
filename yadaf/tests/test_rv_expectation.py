# Copyright 2018, Université de Technologie de Compiègne, France,
#                 Jean-Benoist Leger <jbleger@hds.utc.fr>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import pytest
from .. import ContinuousProbabilityDistribution
import numpy as np
import scipy as sp
import scipy.stats
from ..cpd import logit, logistic

check_distribs = (
    [
        (f"N({m},{s})", sp.stats.norm(m, s))
        for m in (-10, -5, -2, -1, 0, 1, 2, 5, 10)
        for s in (
            1 / 100,
            1 / 50,
            1 / 20,
            1 / 10,
            1 / 5,
            1 / 2,
            1,
            2,
            5,
            10,
            20,
            50,
            100,
        )
    ]
    + [
        (f"T({d})", sp.stats.t(d))
        for d in (1, 2, 3, 4, 5, 7, 10, 15, 20, 30, 40, 50, 100, 200, 500)
    ]
    + [
        (f"chisq({d})", sp.stats.chi2(d))
        for d in (3, 4, 5, 7, 10, 15, 20, 30, 40, 50, 100, 200)
    ]
)

mean_allowed_error = 1e-4


@pytest.mark.parametrize("dist", check_distribs)
def test_mean(dist):
    dname, distrib = dist
    if isinstance(distrib.dist, sp.stats._continuous_distns.t_gen):
        if distrib.args[0] < 2:
            return None
    v = ContinuousProbabilityDistribution(distrib)
    assert np.abs(v.mean() - distrib.mean()) < mean_allowed_error


var_allowed_error = 2e-3


@pytest.mark.parametrize("dist", check_distribs)
def test_var(dist):
    dname, distrib = dist
    if isinstance(distrib.dist, sp.stats._continuous_distns.t_gen):
        if distrib.args[0] < 3:
            return None
    v = ContinuousProbabilityDistribution(distrib)
    assert np.abs(v.var() / distrib.var() - 1) < var_allowed_error


moment_allowed_error = 2e-3


@pytest.mark.parametrize("dist", check_distribs)
def test_moment4(dist):
    dname, distrib = dist
    if isinstance(distrib.dist, sp.stats._continuous_distns.t_gen):
        if distrib.args[0] < 6:
            return None
    v = ContinuousProbabilityDistribution(distrib)
    assert np.abs(v.moment(4) / distrib.moment(4) - 1) < moment_allowed_error
