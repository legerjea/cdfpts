# Copyright 2018, Université de Technologie de Compiègne, France,
#                 Jean-Benoist Leger <jbleger@hds.utc.fr>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import pytest
from .. import ContinuousProbabilityDistribution
import numpy as np
import scipy as sp
import scipy.stats
from ..cpd import logit, logistic

quantiles_to_check = np.arange(1, 10000) / 10000
safe_quantiles_to_check = quantiles_to_check[.5-np.abs(quantiles_to_check-.5)>.01]
lquantiles_to_check = np.linspace(-20, 20, 10000)
quantile_allowed_error = 1e-3
lquantile_allowed_error = 5e-3
lratio_pdf_allowed_error = 2e-1
dpdf_allowed_error = 1e-1

check_distribs = (
    [
        (f"N({m},{s})", sp.stats.norm(m, s))
        for m in (-20, -10, -5, -1, 0, 1, 5, 10, 20)
        for s in (1 / 100, 1 / 10, 1, 10, 100)
    ]
    + [
        (f"T({d})", sp.stats.t(d))
        for d in (3, 4, 5, 7, 10, 15, 20, 30, 40, 50, 100, 200, 500)
    ]
    + [
        (f"chisq({d})", sp.stats.chi2(d))
        for d in (3, 4, 5, 7, 10, 15, 20, 30, 40, 50, 100, 200, 500)
    ]
    + [
        (f"expon({s})", sp.stats.expon(scale=s))
        for s in (0.1, 0.2, 0.5, 1, 2, 5, 10, 20, 50, 100, 200, 500)
    ]
)

check_distribs_dpdf = [
    (f"N({m},{s})", sp.stats.norm(m, s))
    for m in np.linspace(-10, 10, 11)
    for s in np.exp(np.linspace(np.log(0.5), np.log(5), 9))
]


@pytest.mark.parametrize("dist", check_distribs)
def test_ppf_linear(dist):
    dname, distrib = dist
    v = ContinuousProbabilityDistribution(distrib)
    error = distrib.cdf(v.ppf(safe_quantiles_to_check)) - safe_quantiles_to_check
    assert (
        np.max(np.abs(error)) < quantile_allowed_error
    ), "ppf evaluation, linscale, distrib = {dname}, error = {error}"


@pytest.mark.parametrize("dist", check_distribs)
def test_cdf_linear(dist):
    dname, distrib = dist
    v = ContinuousProbabilityDistribution(distrib)
    error = v.cdf(distrib.ppf(quantiles_to_check)) - quantiles_to_check
    assert (
        np.max(np.abs(error)) < quantile_allowed_error
    ), f"cdf evaluation, linscale, distrib = {dname}, error = {error}"


@pytest.mark.parametrize("dist", check_distribs)
def test_pdf_linear(dist):
    dname, distrib = dist
    v = ContinuousProbabilityDistribution(distrib)
    x = distrib.ppf(safe_quantiles_to_check)
    error = np.log(v.pdf(x)) - np.log(distrib.pdf(x))
    assert (
        np.max(np.abs(error)) < lratio_pdf_allowed_error
    ), f"pdf evaluation, linscale, distrib = {dname}, error = {error}"



@pytest.mark.parametrize("dist", check_distribs_dpdf)
def test_dpdf(dist):
    dname, distrib = dist
    v = ContinuousProbabilityDistribution(distrib)
    x = np.linspace(distrib.ppf(1e-5), distrib.ppf(1 - 1e-5), 100_000)
    pdf = v.pdf(x)
    dpdf = v.dpdf(x)
    approx_dpdf = (pdf[2:] - pdf[0:-2]) / (x[2:] - x[0:-2])
    assert (
        np.max(np.abs(approx_dpdf - dpdf[1:-1])) / np.max(pdf) < dpdf_allowed_error
    ), f"dpdf evaluation, distrib = {dname}"
