# Copyright 2018-2020, Université de Technologie de Compiègne, France,
#                      Jean-Benoist Leger <jbleger@hds.utc.fr>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import pytest
from .. import ContinuousProbabilityDistribution
from .. import hellinger_distance
import numpy as np
import scipy as sp
import scipy.stats

underexp = lambda x: np.exp(x) if x > -700 else 0.0

h2norm = lambda m1, s1, m2, s2: 1 - np.sqrt(
    2 * s1 * s2 / (s1 ** 2 + s2 ** 2)
) * underexp(-0.25 * (m1 - m2) ** 2 / (s1 ** 2 + s2 ** 2))

values_to_check_norm = [
    (m, s)
    for m in (-16, -4, -1, 0, 1, 4, 16)
    for s in (1 / 32, 1 / 16, 1 / 4, 1, 4, 16, 32)
]

hellinger_allowed_error = 1e-3


@pytest.mark.parametrize("p1", values_to_check_norm)
@pytest.mark.parametrize("p2", values_to_check_norm)
def test_hellinger_normal(p1, p2):
    m1, s1 = p1
    m2, s2 = p2
    X = ContinuousProbabilityDistribution(sp.stats.norm(m1, s1))
    Y = ContinuousProbabilityDistribution(sp.stats.norm(m2, s2))
    theo_value = h2norm(m1, s1, m2, s2)
    assert (
        np.abs(hellinger_distance(X, Y) ** 2 - theo_value) < hellinger_allowed_error
    ), f"N({m1},{s2}) vs N({m2},{s2})"

