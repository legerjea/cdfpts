# Copyright 2018, Université de Technologie de Compiègne, France,
#                 Jean-Benoist Leger <jbleger@hds.utc.fr>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import pytest
from .. import ContinuousProbabilityDistribution
from .. import hellinger_distance
import numpy as np
import scipy as sp
import scipy.stats
from ..cpd import logit, logistic
import functools

sum_to_check = (
    [
        (
            (
                ContinuousProbabilityDistribution(sp.stats.norm(m1, s1)),
                ContinuousProbabilityDistribution(sp.stats.norm(m2, s2)),
            ),
            ContinuousProbabilityDistribution(
                sp.stats.norm(m1 + m2, np.sqrt(s1 ** 2 + s2 ** 2))
            ),
            f"N({m1},{s1})+N({m2},{s2})",
        )
        for m1 in (-5, -2, -1, 0, 1, 2)
        for m2 in (-5, -2, -1, 0, 1, 2)
        for s1 in (1 / 8, 1 / 4, 1, 4, 8)
        for s2 in (1 / 16, 1 / 4, 1, 4, 16)
    ]
    + [
        (
            (ContinuousProbabilityDistribution(sp.stats.norm(m, s)), c),
            ContinuousProbabilityDistribution(sp.stats.norm(m + c, s)),
            f"N({m},{s})+{c}",
        )
        for m in (-5, -2, -1, 0, 1, 5, 10)
        for s in (1 / 16, 1 / 4, 1 / 2, 1, 2, 4, 8)
        for c in (-8, -2, -1, 0, 1, 2, 4)
    ]
    + [
        (
            (c1, ContinuousProbabilityDistribution(sp.stats.norm(m, s)), c2),
            ContinuousProbabilityDistribution(sp.stats.norm(m + c1 + c2, s)),
            f"{c1}+N({m},{s})+{c2}",
        )
        for m in (-5, -2, -1, 0, 1, 3, 5)
        for s in (1 / 16, 1 / 4, 1 / 2, 1, 2, 4, 8)
        for c1 in (-10, -2, -1, 0, 1, 2, 10)
        for c2 in (-10, -2, -1, 0, 1, 2, 10)
    ]
)


@pytest.mark.parametrize("dists", sum_to_check)
def test_sums(dists):
    dists_origs, dist_target, testname = dists
    dsum = functools.reduce(lambda x, y: x + y, dists_origs)
    assert hellinger_distance(dsum, dist_target) < 1e-3, testname


prod_to_check = (
    [
        (
            (ContinuousProbabilityDistribution(sp.stats.norm(m, s)), c),
            ContinuousProbabilityDistribution(sp.stats.norm(m * c, np.abs(c) * s)),
            f"N({m},{s})*{c}",
        )
        for m in (-5, -1, 0, 1, 5)
        for s in (1 / 4, 1 / 2, 1, 2, 4)
        for c in (-2, -1, 1, 2)
    ]
    + [
        (
            (c1, ContinuousProbabilityDistribution(sp.stats.norm(m, s)), c2),
            ContinuousProbabilityDistribution(
                sp.stats.norm(m * c1 * c2, s * np.abs(c1 * c2))
            ),
            f"{c1}*N({m},{s})*{c2}",
        )
        for m in (-5, -1, 0, 1, 5)
        for s in (1 / 4, 1 / 2, 1, 2, 4)
        for c1 in (-2, -1, 1, 2)
        for c2 in (-2, -1, 1, 2)
    ]
    + [
        (
            (ContinuousProbabilityDistribution(sp.stats.expon(scale=s)), c),
            ContinuousProbabilityDistribution(sp.stats.expon(scale=s * c)),
            f"E({s})*{c}",
        )
        for s in (1 / 8, 1 / 4, 1 / 2, 1, 2, 4, 8)
        for c in (1 / 4, 1 / 2, 1, 2, 4)
    ]
)


@pytest.mark.parametrize("dists", prod_to_check)
def test_prodss(dists):
    dists_origs, dist_target, testname = dists
    dsum = functools.reduce(lambda x, y: x * y, dists_origs)
    assert hellinger_distance(dsum, dist_target) < 1e-3, testname


dists_tcl = [
    sp.stats.t(6),
    sp.stats.t(10),
    sp.stats.chi2(3),
]


@pytest.mark.parametrize("dist", dists_tcl)
def test_TCL(dist):
    X = ContinuousProbabilityDistribution(dist,nbpts=1001)
    N = ContinuousProbabilityDistribution(sp.stats.norm,nbpts=1001)

    dold = hellinger_distance(X, N)
    sold = X.skewness()
    kold = X.kurtosis()
    n = 1
    m = dist.mean()
    v = dist.var()
    while n < 17000:
        X = X + X
        n *= 2
        d = hellinger_distance((X - n * m) / np.sqrt(n * v), N)
        s = X.skewness()
        k = X.kurtosis()
        assert d < dold
        assert np.abs(k - 3) < np.abs(kold - 3)
        dold, sold, kold = d, s, k
    assert d < 1e-2
    assert np.abs(s) < 5e-2
    assert np.abs(k - 3) < 1e-2
