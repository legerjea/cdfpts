# Copyright 2018-2020, Université de Technologie de Compiègne, France,
#                      Jean-Benoist Leger <jbleger@hds.utc.fr>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import pytest
from .. import ContinuousProbabilityDistribution
from .. import hellinger_distance
from .. import bayes_lin
import numpy as np
import scipy as sp
import scipy.stats

np.seterr(all="raise")

scales_to_check = [
    (scale, nbpts) for scale in ("logit", "linear", "linlogitmixture", "xylinear") for nbpts in (201, 301, 401, 1001)
]


@pytest.mark.parametrize("p1", scales_to_check)
@pytest.mark.parametrize("p2", scales_to_check)
def test_scales_bayes(p1, p2):
    scale1, nbpts1 = p1
    scale2, nbpts2 = p2
    prior = ContinuousProbabilityDistribution(
        sp.stats.norm(0, 1), scale=scale1, nbpts=nbpts1
    )
    noise = ContinuousProbabilityDistribution(
        sp.stats.norm(1, 0.5), scale=scale2, nbpts=nbpts2
    )
    posterior = bayes_lin(prior, noise, 0)
    if scale1 == scale2:
        assert posterior._scale == scale1
    else:
        assert posterior._scale == "xylinear"
    assert len(posterior._xpts) == max(nbpts1, nbpts2)


@pytest.mark.parametrize("p1", scales_to_check)
@pytest.mark.parametrize("p2", scales_to_check)
def test_scales_add(p1, p2):
    scale1, nbpts1 = p1
    scale2, nbpts2 = p2
    A = ContinuousProbabilityDistribution(
        sp.stats.norm(0, 1), scale=scale1, nbpts=nbpts1
    )
    B = ContinuousProbabilityDistribution(
        sp.stats.norm(1, 0.5), scale=scale2, nbpts=nbpts2
    )
    R = A + B
    if scale1 == scale2:
        assert R._scale == scale1
    else:
        assert R._scale == "xylinear"
    assert len(R._xpts) == max(nbpts1, nbpts2)


@pytest.mark.parametrize("p", scales_to_check)
def test_scales_apply_monotonic_function(p):
    scale, nbpts = p
    A = ContinuousProbabilityDistribution(
        sp.stats.norm(0, 1), scale=scale, nbpts=nbpts
    )
    R = A.apply_monotonic_function(lambda x: -x)
    assert A._scale == R._scale
    assert len(R._xpts) == len(R._xpts)
