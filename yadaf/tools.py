# Copyright 2018-2020, Université de Technologie de Compiègne, France,
#                      Jean-Benoist Leger <jbleger@hds.utc.fr>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import types
import collections.abc
import numpy as np
from .cpd import ContinuousProbabilityDistribution
from ._internal import hellinger
from ._stuff import cond_lin, cond_likli, cond_likli_gm


def hellinger_distance(
    X: ContinuousProbabilityDistribution, Y: ContinuousProbabilityDistribution
):
    """Compute the Hellinger distance.

    `X` and `Y` have to be instance of ContinuousProbabilityDistribution.
    """

    if not isinstance(X, ContinuousProbabilityDistribution):
        raise TypeError(
            f"X should be of type {ContinuousProbabilityDistribution.__name__} ; actual type: {type(X)}"
        )
    if not isinstance(Y, ContinuousProbabilityDistribution):
        raise TypeError(
            f"Y should be of type {ContinuousProbabilityDistribution.__name__} ; actual type: {type(Y)}"
        )

    return np.sqrt(hellinger.hellinger_distance2(X._xpts, X._lypts, Y._xpts, Y._lypts))


def bayes_lin(
    Z: ContinuousProbabilityDistribution,
    U: ContinuousProbabilityDistribution,
    x,
    hx=None,
    hz=None,
    hu=None,
):
    """Compute the posterior (Z|X=x). Where X|Z=z = h_x^{-1}(h_z(z) + h_u(U)).

    Arguments:
     - `Z` the prior distrib. Must be a ContinuousProbabilityDistribution.
     - `U`. A ContinuousProbabilityDistribution.
     - links functions, hx, hu, hz. 
       Each have to be a 2-elements tuple, containing the direct function and
       the reciprocical.

    All function have to be monotonous.

    If `hx`, `hu` or `hz` is None, identity function is used for this function.

    Examples:
     - Additive noise: X|Z=z ~ Normal(z,s^2).
       - Rewrite as X|Z=z = z + U, 
         with U ~ Normal(0,s^2).
       - Rewrite as X|Z=z = h_x^{-1}(hz(z)+hu(U)),
         with U ~ Normal(0,s^2),
         with h_x = h_z = h_u = identity_function. Defaults args, None.
     - Multiplicative noise: X|Z=z ~ z×LogNormal(0,s^2)
       - Rewrite as X|Z=z = exp(log(z) + U)
         with U ~ Normal(0,s^2).
       - Arguments:
         - h_x^{-1}=log. Therefore, h_x=exp. So we have to pass
           `hx = (np.exp,np.log)`.
         - h_z=exp. Therefore, h_z^{-1}=log. So we have to pass
           `hz = (np.exp,np.log)`.
         - h_u=identity. Default arg.
    """
    if not isinstance(Z, ContinuousProbabilityDistribution):
        raise TypeError(
            f"Z should be of type {ContinuousProbabilityDistribution.__name__} ; actual type: {type(Z)}"
        )
    if not isinstance(U, ContinuousProbabilityDistribution):
        raise TypeError(
            f"Y should be of type {ContinuousProbabilityDistribution.__name__} ; actual type: {type(U)}"
        )

    if hx is None:
        hx = (lambda w: w, lambda w: w)
    if hu is None:
        hu = (lambda w: w, lambda w: w)
    if hz is None:
        hz = (lambda w: w, lambda w: w)

    return cond_lin(
        Z.apply_monotonic_function(hz[0]), hx[1](x) - U.apply_monotonic_function(hu[0])
    ).apply_monotonic_function(hz[1])


def bayes_likli(Z_prior: ContinuousProbabilityDistribution, likli):
    """Compute the posterior by appling the Bayes theorem.

    Returns the posterior distrib which have the following pdf:
    
                               Z_prior.pdf(z)×likli(z)
       pdf(z) =  ------------------------------------------------------
                 integrate(Z_prior.pdf(t)×likli(t), t from -inf to inf)
    
    Parameters
    ----------
     - Z_prior : ContinuousProbabilityDistribution
       The prior distribution
     - likli : tuple of 2 functions, or function returning a tuple
       Can be a tuple with the function returning the liklihood and the function
       returning the derivative of the liklihood, or a function returing a tuple
       the the liklihood and the derivative of the liklihood. Examples (for
       where liklihood is the indicator function in [0,1], the derivative of the
       liklihood is 0 almost everywhere):
        - (lambda x: 1 if 0<x<1 else 0, lambda x: 0)
        - lambda x: (1 if 0<x<1 else 0, 0)
    """
    if not isinstance(Z_prior, ContinuousProbabilityDistribution):
        raise TypeError(
            f"Z_prior should be of type {ContinuousProbabilityDistribution.__name__} ; actual type: {type(Z_prior)}"
        )
    if isinstance(likli, types.FunctionType) and (
        lambda ex: (
            hasattr(ex, "__len__")
            and len(ex) == 2
            and isinstance(ex[0], (int, float))
            and isinstance(ex[1], (int, float))
        )
    )(likli(Z_prior.mean())):
        used_likli = lambda z: likli(z)
    elif (
        hasattr(likli, "__len__")
        and len(likli) == 2
        and isinstance(likli[0], types.FunctionType)
        and isinstance(likli[1], types.FunctionType)
        and isinstance(likli[0](Z_prior.mean()), (int, float))
        and isinstance(likli[1](Z_prior.mean()), (int, float))
    ):
        used_likli = lambda z: (likli[0](z), likli[1](z))
    else:
        raise TypeError(
            f"likli should be a function returning a 2-scalars tuple (or equivalent) or a tuple of two function returning scalars."
        )

    return cond_likli(Z_prior, used_likli)


def bayes_likli_gm(
    Z_prior: ContinuousProbabilityDistribution, likli_gm
):
    """Compute the posterior by appling the Bayes theorem. When the liklihood is
    a gaussian mixture.

    Returns the posterior distrib which have the following pdf:
    
                               Z_prior.pdf(z)×likli(z)
       pdf(z) =  ------------------------------------------------------
                 integrate(Z_prior.pdf(t)×likli(t), t from -inf to inf)

    with likli defined as follow:

       likli = lambda z: sum( K*exp(-1/2*(z-c)*prec  for (K, c, prec) in likli_gm)

    This is equivalent with bayes_likli function, but baster to compute as the
    computation of liklihood is made in cython allowing to release the GIL.
    
    Parameters
    ----------
     - Z_prior : ContinuousProbabilityDistribution
       The prior distribution
     - likli_gm : list of tuple of 3 scalars.
       Description of each components of gaussian mixture. Be carefull, the
       component must be the amplitude, the center, and the precision. The
       precision is the inverse of variance.
    """
    if not isinstance(Z_prior, ContinuousProbabilityDistribution):
        raise TypeError(
            f"Z_prior should be of type {ContinuousProbabilityDistribution.__name__} ; actual type: {type(Z_prior)}"
        )
    gm_ok = False
    if isinstance(likli_gm, collections.abc.Iterable):
        llikli_gm = list(likli_gm)
        if all(
            (hasattr(x, "__len__") and len(x) == 3 and x[0] >= 0 and x[2] >= 0)
            for x in llikli_gm
        ):
            gm_K = np.array([x[0] for x in llikli_gm], dtype=np.float64)
            gm_c = np.array([x[1] for x in llikli_gm], dtype=np.float64)
            gm_prec = np.array([x[2] for x in llikli_gm], dtype=np.float64)
            gm_ok = True
    if not gm_ok:
        raise TypeError(
            f"likli_gm must be a interable of 3-elements iterables, amplitude and precision must be non negative."
        )

    return cond_likli_gm(Z_prior, gm_K, gm_c, gm_prec)
