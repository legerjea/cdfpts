# Copyright 2019, Université de Technologie de Compiègne, France,
#                 Jean-Benoist Leger <jbleger@hds.utc.fr>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import functools


class lazy_cached_property:
    """
    Lazy evaluation for object attribute.
    Must represent non mutable data.
    """

    def __init__(self, get):
        self._get = get
        self._attrname = "_lazy_property_" + get.__name__
        functools.update_wrapper(self, get)

    def __get__(self, obj, typ):
        if obj is None:
            return self

        if not hasattr(obj, self._attrname):
            setattr(obj, self._attrname, self._get(obj))
        return getattr(obj, self._attrname)

    def __set__(self, obj, value):
        raise AttributeError("can't set attribute")


def lazy_cached_method_getter(method):
    """
    Decoration for caching method getter.
    Must get non mutable data.
    """

    attrname = "_lazy_cached_method_getter_" + method.__name__

    def wrapped(self):
        if not hasattr(self, attrname):
            setattr(self, attrname, method(self))
        return getattr(self, attrname)

    functools.update_wrapper(wrapped, method)
    return wrapped
