# Copyright 2018, Université de Technologie de Compiègne, France, 
#                 Jean-Benoist Leger <jbleger@hds.utc.fr>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# cython: language_level=3
# cython: boundscheck=False
# cython: wraparound=False
# cython: cdivision=True

# distutils: language = c++

cimport cython
import numpy as np
from libc.math cimport (sqrt, exp)
from libc.string cimport memset

include "common.pxi"

cdef struct hellinger_materials_t:
    double* xptsA
    double* lyptsA
    Py_ssize_t NA
    double* xptsB
    double* lyptsB
    Py_ssize_t NB
    Py_ssize_t tiA
    Py_ssize_t tiB


cdef void hellinger_fun(couple* ret, void* materials, double x) nogil:
    cdef hellinger_materials_t* params = <hellinger_materials_t*>materials
    cdef interpolated_t interpA, interpB
    cdef cpdp_t cpdpA, cpdpB
    interpol(&interpA, params.xptsA, params.lyptsA, params.NA, x, 1, params.tiA, 1)
    interpol(&interpB, params.xptsB, params.lyptsB, params.NB, x, 1, params.tiB, 1)
    interp2cpdp(&cpdpA, &interpA)
    interp2cpdp(&cpdpB, &interpB)
    
    cdef double diff_sqrtpdf
    cdef double diff_dpdfosqrtpdf
    
    sqrtpdfA = sqrt(cpdpA.pdf)
    sqrtpdfB = sqrt(cpdpB.pdf)
    if cpdpA.pdf>0:
        if cpdpB.pdf>0:
            diff_sqrtpdf = sqrtpdfA-sqrtpdfB
            diff_dpdfosqrtpdf = cpdpA.dpdf/sqrtpdfA-cpdpB.dpdf/sqrtpdfB
            
            ret.val = diff_sqrtpdf*diff_sqrtpdf
            ret.der = diff_sqrtpdf*diff_dpdfosqrtpdf
        else:
            ret.val = cpdpA.pdf
            ret.der = cpdpA.dpdf
    else:
        if cpdpB.pdf>0:
            ret.val = cpdpB.pdf
            ret.der = cpdpB.dpdf
        else:
            ret.val = 0
            ret.der = 0


def hellinger_distance2(double [:] xptsA, double [:] lyptsA, double [:] xptsB,
        double [:] lyptsB):
    cdef Py_ssize_t NA = xptsA.shape[0]
    cdef Py_ssize_t NB = xptsB.shape[0]
    cdef double exterior = 5
    cdef double inf = float("inf")

    cdef int iA = -1
    cdef int iB = -1

    cdef double value = 0

    cdef int nextiA, nextiB
    cdef Py_ssize_t tiA,tiB
    cdef double xstart=0, xstop=0, xstartA=0, xstartB=0, xstopA=0, xstopB=0

    cdef hellinger_materials_t materials

    materials.xptsA = &xptsA[0]
    materials.lyptsA = &lyptsA[0]
    materials.NA = NA
    materials.xptsB = &xptsB[0]
    materials.lyptsB = &lyptsB[0]
    materials.NB = NB

    cdef double tmp_value
    while iA<NA+1 or iB<NB+1:

        if iA == -1:
            xstartA,xstopA = -inf,xptsA[0]-exterior*(xptsA[1]-xptsA[0])/(lyptsA[1]-lyptsA[0])
        elif iA == 0:
            xstartA,xstopA = xptsA[0]-exterior*(xptsA[1]-xptsA[0])/(lyptsA[1]-lyptsA[0]),xptsA[0]
        elif iA == NA:
            xstartA,xstopA = xptsA[NA-1],xptsA[NA-1]+exterior*(xptsA[NA-2]-xptsA[NA-1])/(lyptsA[NA-2]-lyptsA[NA-1])
        elif iA == NA+1:
            xstartA,xstopA = xptsA[NA-1]+exterior*(xptsA[NA-2]-xptsA[NA-1])/(lyptsA[NA-2]-lyptsA[NA-1]),inf
        else:
            xstartA,xstopA = xptsA[iA-1],xptsA[iA]

        if iB == -1:
            xstartB,xstopB = -inf,xptsB[0]-exterior*(xptsB[1]-xptsB[0])/(lyptsB[1]-lyptsB[0])
        elif iB == 0:
            xstartB,xstopB = xptsB[0]-exterior*(xptsB[1]-xptsB[0])/(lyptsB[1]-lyptsB[0]),xptsB[0]
        elif iB == NB:
            xstartB,xstopB = xptsB[NB-1],xptsB[NB-1]+exterior*(xptsB[NB-2]-xptsB[NB-1])/(lyptsB[NB-2]-lyptsB[NB-1])
        elif iB == NB+1:
            xstartB,xstopB = xptsB[NB-1]+exterior*(xptsB[NB-2]-xptsB[NB-1])/(lyptsB[NB-2]-lyptsB[NB-1]),inf
        else:
            xstartB,xstopB = xptsB[iB-1],xptsB[iB]

        if iA == -1 and iB == -1:
            if xstopA<xstopB:
                iA+=1
                continue
            iB+=1
            continue

        if xstartA>xstartB:
            xstart = xstartA
        else:
            xstart = xstartB

        if xstopA<xstopB:
            xstop = xstopA
            nextiA,nextiB = iA+1,iB
        else:
            xstop = xstopB
            nextiA,nextiB = iA,iB+1

        tiA = 0 if iA<=0 else NA if iA>=NA else iA
        tiB = 0 if iB<=0 else NB if iB>=NB else iB

        materials.tiA = tiA
        materials.tiB = tiB
        
        tmp_value = _integrate(&hellinger_fun, &materials, xstart, xstop)
        value+=tmp_value

        iA,iB = nextiA,nextiB

    return .5*value
