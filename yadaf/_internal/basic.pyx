# Copyright 2018-2020, Université de Technologie de Compiègne, France,
#                      Jean-Benoist Leger <jbleger@hds.utc.fr>
#                      Philippe Xu <philippe.xu@hds.utc.fr>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# cython: language_level=3
# cython: boundscheck=False
# cython: wraparound=False
# cython: cdivision=True

# distutils: language = c++

import numpy as np
cimport cython
from libc.math cimport (log, exp, abs, sqrt)
from libc.string cimport memset


include "common.pxi"

cdef double ppf_one(double [:] xpts, double [:] lypts, double p):
    cdef double lp = logit(p)
    cdef Py_ssize_t i = bisect(&lypts[0], xpts.shape[0], lp)
    cdef Py_ssize_t i1,i2
    if i==0 or i==xpts.shape[0]:
        if i==0:
            i1,i2 = 0,1
        else:
            i1,i2 = xpts.shape[0]-1,xpts.shape[0]-2
        return (lp-lypts[i1])/(lypts[i2]-lypts[i1])*(xpts[i2]-xpts[i1]) + xpts[i1]
    cdef double x = 0
    cdef double a = xpts[i-1], b = xpts[i]
    x = (a+b)/2
    cdef interpolated_t interp
    cdef double lx
    while True:
        interpol(&interp, &xpts[0], &lypts[0], xpts.shape[0], x, 1, i, 1)
        lx = interp.value
        if lx>lp:
            b = x
        else:
            a = x
        x = (a+b)/2
        if a==x or b==x:
            return x


def ppf_scalar(double [:] xpts, double [:] lypts, double p):
    if xpts is None: raise ValueError
    if lypts is None: raise ValueError
    return ppf_one(xpts, lypts, p)


def ppf_vector(double [:] xpts, double [:] lypts, double [:] p):
    if xpts is None: raise ValueError
    if lypts is None: raise ValueError
    cdef double [:] res = np.zeros(p.shape[0])
    cdef Py_ssize_t i = 0
    for i in range(p.shape[0]):
        res[i] = ppf_one(xpts, lypts, p[i])
    return res


def cdf_scalar(double [:] xpts, double [:] lypts, double x):
    if xpts is None: raise ValueError
    if lypts is None: raise ValueError
    cdef interpolated_t interp
    cdef cpdp_t cpdp
    interpol(&interp, &xpts[0], &lypts[0], xpts.shape[0], x, 0, 0, 1)
    return interp2cdf(&interp)


def cdf_vector(double [:] xpts, double [:] lypts, double [:] x):
    if xpts is None: raise ValueError
    if lypts is None: raise ValueError
    cdef double [:] res = np.zeros(x.shape[0])
    cdef Py_ssize_t i = 0
    cdef interpolated_t interp
    cdef cpdp_t cpdp
    for i in range(x.shape[0]):
        interpol(&interp, &xpts[0], &lypts[0], xpts.shape[0], x[i], 0, 0, 1)
        res[i] = interp2cdf(&interp)
    return res


def logcdf_scalar(double [:] xpts, double [:] lypts, double x):
    if xpts is None: raise ValueError
    if lypts is None: raise ValueError
    cdef interpolated_t interp
    cdef cpdp_t cpdp
    interpol(&interp, &xpts[0], &lypts[0], xpts.shape[0], x, 0, 0, 1)
    return interp2logcdf(&interp)


def logcdf_vector(double [:] xpts, double [:] lypts, double [:] x):
    if xpts is None: raise ValueError
    if lypts is None: raise ValueError
    cdef double [:] res = np.zeros(x.shape[0])
    cdef Py_ssize_t i = 0
    cdef interpolated_t interp
    cdef cpdp_t cpdp
    for i in range(x.shape[0]):
        interpol(&interp, &xpts[0], &lypts[0], xpts.shape[0], x[i], 0, 0, 1)
        res[i] = interp2logcdf(&interp)
    return res


def sf_scalar(double [:] xpts, double [:] lypts, double x):
    if xpts is None: raise ValueError
    if lypts is None: raise ValueError
    cdef interpolated_t interp
    cdef cpdp_t cpdp
    interpol(&interp, &xpts[0], &lypts[0], xpts.shape[0], x, 0, 0, 1)
    return interp2sf(&interp)


def sf_vector(double [:] xpts, double [:] lypts, double [:] x):
    if xpts is None: raise ValueError
    if lypts is None: raise ValueError
    cdef double [:] res = np.zeros(x.shape[0])
    cdef Py_ssize_t i = 0
    cdef interpolated_t interp
    cdef cpdp_t cpdp
    for i in range(x.shape[0]):
        interpol(&interp, &xpts[0], &lypts[0], xpts.shape[0], x[i], 0, 0, 1)
        res[i] = interp2sf(&interp)
    return res


def logsf_scalar(double [:] xpts, double [:] lypts, double x):
    if xpts is None: raise ValueError
    if lypts is None: raise ValueError
    cdef interpolated_t interp
    cdef cpdp_t cpdp
    interpol(&interp, &xpts[0], &lypts[0], xpts.shape[0], x, 0, 0, 1)
    return interp2logsf(&interp)


def logsf_vector(double [:] xpts, double [:] lypts, double [:] x):
    if xpts is None: raise ValueError
    if lypts is None: raise ValueError
    cdef double [:] res = np.zeros(x.shape[0])
    cdef Py_ssize_t i = 0
    cdef interpolated_t interp
    cdef cpdp_t cpdp
    for i in range(x.shape[0]):
        interpol(&interp, &xpts[0], &lypts[0], xpts.shape[0], x[i], 0, 0, 1)
        res[i] = interp2logsf(&interp)
    return res


def logitcdf_scalar(double [:] xpts, double [:] lypts, double x):
    if xpts is None: raise ValueError
    if lypts is None: raise ValueError
    cdef interpolated_t interp
    cdef cpdp_t cpdp
    interpol(&interp, &xpts[0], &lypts[0], xpts.shape[0], x, 0, 0, 1)
    return interp2logitcdf(&interp)


def logitcdf_vector(double [:] xpts, double [:] lypts, double [:] x):
    if xpts is None: raise ValueError
    if lypts is None: raise ValueError
    cdef double [:] res = np.zeros(x.shape[0])
    cdef Py_ssize_t i = 0
    cdef interpolated_t interp
    cdef cpdp_t cpdp
    for i in range(x.shape[0]):
        interpol(&interp, &xpts[0], &lypts[0], xpts.shape[0], x[i], 0, 0, 1)
        res[i] = interp2logitcdf(&interp)
    return res


def pdf_scalar(double [:] xpts, double [:] lypts, double x):
    if xpts is None: raise ValueError
    if lypts is None: raise ValueError
    cdef interpolated_t interp
    cdef cpdp_t cpdp
    interpol(&interp, &xpts[0], &lypts[0], xpts.shape[0], x, 0, 0, 1)
    return interp2pdf(&interp)


def pdf_vector(double [:] xpts, double [:] lypts, double [:] x):
    if xpts is None: raise ValueError
    if lypts is None: raise ValueError
    cdef double [:] res = np.zeros(x.shape[0])
    cdef Py_ssize_t i = 0
    cdef interpolated_t interp
    cdef cpdp_t cpdp
    for i in range(x.shape[0]):
        interpol(&interp, &xpts[0], &lypts[0], xpts.shape[0], x[i], 0, 0, 1)
        res[i] = interp2pdf(&interp)
    return res


def logpdf_scalar(double [:] xpts, double [:] lypts, double x):
    if xpts is None: raise ValueError
    if lypts is None: raise ValueError
    cdef interpolated_t interp
    cdef cpdp_t cpdp
    interpol(&interp, &xpts[0], &lypts[0], xpts.shape[0], x, 0, 0, 1)
    return interp2logpdf(&interp)


def logpdf_vector(double [:] xpts, double [:] lypts, double [:] x):
    if xpts is None: raise ValueError
    if lypts is None: raise ValueError
    cdef double [:] res = np.zeros(x.shape[0])
    cdef Py_ssize_t i = 0
    cdef interpolated_t interp
    cdef cpdp_t cpdp
    for i in range(x.shape[0]):
        interpol(&interp, &xpts[0], &lypts[0], xpts.shape[0], x[i], 0, 0, 1)
        res[i] = interp2logpdf(&interp)
    return res


def dpdf_scalar(double [:] xpts, double [:] lypts, double x):
    if xpts is None: raise ValueError
    if lypts is None: raise ValueError
    cdef interpolated_t interp
    cdef cpdp_t cpdp
    interpol(&interp, &xpts[0], &lypts[0], xpts.shape[0], x, 0, 0, 1)
    return interp2dpdf(&interp)


def dpdf_vector(double [:] xpts, double [:] lypts, double [:] x):
    if xpts is None: raise ValueError
    if lypts is None: raise ValueError
    cdef double [:] res = np.zeros(x.shape[0])
    cdef Py_ssize_t i = 0
    cdef interpolated_t interp
    cdef cpdp_t cpdp
    for i in range(x.shape[0]):
        interpol(&interp, &xpts[0], &lypts[0], xpts.shape[0], x[i], 0, 0, 1)
        res[i] = interp2dpdf(&interp)
    return res


cdef struct entropy_materials_t:
    double* xpts
    double* lypts
    Py_ssize_t N
    Py_ssize_t i


cdef void entropy_fun(couple* ret, void* materials, double x) nogil:
    cdef entropy_materials_t* params = <entropy_materials_t*>materials
    cdef interpolated_t interp
    cdef cpdp_t cpdp
    interpol(&interp, params.xpts, params.lypts, params.N, x, 1, params.i, 1)
    interp2cpdp(&cpdp, &interp)
    if cpdp.pdf == 0.:
        ret.val = 0.
        ret.der = 0.
    else:
        ret.val = cpdp.pdf * log(cpdp.pdf)
        ret.der = cpdp.dpdf * (log(cpdp.pdf) + 1)


def entropy(double [:] xpts, double [:] lypts):

    cdef Py_ssize_t N = xpts.shape[0]
    cdef Py_ssize_t i = 0
    cdef double value = 0
    cdef double xstart=0,xstop=0
    cdef entropy_materials_t materials

    materials.xpts = &xpts[0]
    materials.lypts = &lypts[0]
    materials.N = xpts.shape[0]

    for i in range(N+1):
        if i == 0:
            xstart,xstop = xpts[0]-5*(xpts[1]-xpts[0])/(lypts[1]-lypts[0]),xpts[0]
        elif i == N:
            xstart,xstop = xpts[N-1],xpts[N-1]+5*(xpts[N-2]-xpts[N-1])/(lypts[N-2]-lypts[N-1])
        else:
            xstart,xstop = xpts[i-1],xpts[i]

        materials.i = i
        value += _integrate(&entropy_fun, &materials, xstart, xstop)
    return -value


cdef struct expectation_monomial_materials_t:
    double* xpts
    double* lypts
    Py_ssize_t N
    Py_ssize_t i
    unsigned int pui
    double c


cdef void expectation_monomial_fun(couple* ret, void* materials, double x) nogil:
    cdef expectation_monomial_materials_t* params = <expectation_monomial_materials_t*>materials
    cdef interpolated_t interp
    cdef cpdp_t cpdp
    interpol(&interp, params.xpts, params.lypts, params.N, x, 1, params.i, 1)
    interp2cpdp(&cpdp, &interp)
    if params.pui==0:
        ret.val = cpdp.pdf
        ret.der = cpdp.dpdf
    elif params.pui==1:
        ret.val = cpdp.pdf * (x-params.c)
        ret.der = cpdp.dpdf * (x-params.c) + cpdp.pdf
    else:
        ret.val = cpdp.pdf * (x-params.c)**params.pui
        ret.der = (cpdp.dpdf * (x-params.c) + cpdp.pdf * params.pui) * (x-params.c)**(params.pui-1)


def expectation(double [:] xpts, double [:] lypts,
        unsigned int pui, double c):

    cdef Py_ssize_t N = xpts.shape[0]
    cdef Py_ssize_t i = 0
    cdef double value = 0
    cdef double xstart=0,xstop=0
    cdef expectation_monomial_materials_t materials

    materials.xpts = &xpts[0]
    materials.lypts = &lypts[0]
    materials.N = xpts.shape[0]
    materials.pui = pui
    materials.c = c

    for i in range(N+1):
        if i == 0:
            xstart,xstop = xpts[0]-5*(xpts[1]-xpts[0])/(lypts[1]-lypts[0]),xpts[0]
        elif i == N:
            xstart,xstop = xpts[N-1],xpts[N-1]+5*(xpts[N-2]-xpts[N-1])/(lypts[N-2]-lypts[N-1])
        else:
            xstart,xstop = xpts[i-1],xpts[i]

        materials.i = i
        value += _integrate(&expectation_monomial_fun, &materials, xstart, xstop)
    return value

cdef struct confint_interpol_intersect_res_t:
    double x
    double cdf

cdef void confint_interpol_intersect(confint_interpol_intersect_res_t* res, double [:] xpts, double [:] lypts, double
        level, Py_ssize_t i, unsigned int ascend, double cdf0, double cdf1) nogil:
    cdef Py_ssize_t N = xpts.shape[0]
    cdef interpolated_t interp
    cdef cpdp_t cpdp
    cdef double x

    cdef double x0, x1
    cdef double xstep
    if i == 0:
        ascend = 1
        xstep = xpts[1]-xpts[0]
        x1 = xpts[0]
        x0 = x1-xstep
        while True:
            interpol(&interp, &xpts[0], &lypts[0], xpts.shape[0], x0, 1, i, 1)
            interp2cpdp(&cpdp,&interp)
            if cpdp.pdf < level:
                break
            x0 -= xstep
    elif i == N:
        ascend = 0
        xstep = xpts[N-1]-xpts[N-2]
        x0 = xpts[N-1]
        x1 = x0+xstep
        while True:
            interpol(&interp, &xpts[0], &lypts[0], xpts.shape[0], x1, 1, i, 1)
            interp2cpdp(&cpdp,&interp)
            if cpdp.pdf < level:
                break
            x1 += xstep
    else:
        x0,x1 = xpts[i-1],xpts[i]

    while True:
        x = (x1+x0)/2
        if x==x0 or x==x1:
            res.x = x
            res.cdf = (cdf0+cdf1)/2
            break
        interpol(&interp, &xpts[0], &lypts[0], xpts.shape[0], x, 1, i, 1)
        interp2cpdp(&cpdp,&interp)
        if ascend :
            if cpdp.pdf > level:
                x1, cdf1 = x, cpdp.cdf
            else:
                x0, cdf0 = x, cpdp.cdf
        else:
            if cpdp.pdf > level:
                x0, cdf0 = x, cpdp.cdf
            else:
                x1, cdf1 = x, cpdp.cdf


cdef struct confint_prob_level_res_t:
    double* bornes_inf
    double* bornes_sup
    Py_ssize_t Ninter
    double prob


cdef void confint_prob_level(
    confint_prob_level_res_t * res, double [:] xpts, double [:] lypts, double level,
) nogil:

    res.Ninter = 0
    res.prob = 0
    cdef Py_ssize_t N = xpts.shape[0]
    cdef interpolated_t interp
    cdef cpdp_t cpdp

    # Récupérer pdf aux points xpts
    cdef double pdf0 = 0
    cdef double cdf0 = 0
    cdef double pdf1
    cdef double cdf1

    cdef confint_interpol_intersect_res_t intersect_res

    for i in range(N):
        # Get pdf0, pdf1, cdf0, cdf1
        interpol(&interp, &xpts[0], &lypts[0], xpts.shape[0], xpts[i], 1, i, 1)
        interp2cpdp(&cpdp,&interp)
        pdf1 = cpdp.pdf
        cdf1 = cpdp.cdf

        if pdf0 <= level and pdf1 > level:
            # ascend
            confint_interpol_intersect(&intersect_res, xpts, lypts, level, i, 1, cdf0, cdf1)
            res.prob -= intersect_res.cdf
            res.bornes_inf[res.Ninter] = intersect_res.x
            res.Ninter += 1
        elif pdf0 > level and pdf1 <= level:
            # descend
            confint_interpol_intersect(&intersect_res, xpts, lypts, level, i, 0, cdf0, cdf1)
            res.prob += intersect_res.cdf
            res.bornes_sup[res.Ninter-1] = intersect_res.x
        pdf0 = pdf1
        cdf0 = cdf1
    if pdf0>level:
        confint_interpol_intersect(&intersect_res, xpts, lypts, level, N, 0, cdf0, 1.0)
        res.prob += intersect_res.cdf
        res.bornes_sup[res.Ninter-1] = intersect_res.x


def confint(double [:] xpts, double [:] lypts, double confidence):
    cdef Py_ssize_t N = xpts.shape[0]
    cdef double a = xpts[N-1]-xpts[0]
    cdef double level_sup = 2/a*sqrt(1-confidence)
    cdef double level_inf = 0
    cdef double level_mid

    cdef confint_prob_level_res_t level_res
    cdef double [:] bornes_inf = np.zeros(N)
    cdef double [:] bornes_sup = np.zeros(N)
    level_res.bornes_inf = &bornes_inf[0]
    level_res.bornes_sup = &bornes_sup[0]

    while True:
        confint_prob_level(&level_res, xpts, lypts, level_sup)
        if level_res.prob < confidence:
            break
        level_inf = level_sup
        level_sup *= 2

    while True:
        level_mid = (level_sup + level_inf)/2
        if level_mid == level_sup or level_mid == level_inf:
            break
        confint_prob_level(&level_res, xpts, lypts, level_mid)
        if level_res.prob < confidence:
            level_sup = level_mid
        else:
            level_inf = level_mid

    return (bornes_inf[:level_res.Ninter], bornes_sup[:level_res.Ninter])

def subsample(double [:] pts, Py_ssize_t Nobj, double regular_factor):
    cdef Py_ssize_t npts = pts.size
    cdef double ptsmax = pts[npts-1]
    cdef double ptsmin = pts[0]

    cdef double [:] ypts = np.zeros(npts)
    cdef Py_ssize_t i
    cdef Py_ssize_t N = int(Nobj * (1+regular_factor))
    cdef double [:] res = np.zeros(N)
    cdef Py_ssize_t k
    cdef double x

    for i in range(npts):
        ypts[i] = (float(i)/(npts-1)+regular_factor*(pts[i]-ptsmin)/(ptsmax-ptsmin))/(1+regular_factor)

    res[0] = pts[0]
    for i in range(1, N-1):
        x = float(i)/(N-1)
        k = bisect(&ypts[0], npts, x)
        res[i] = pts[k-1]+(pts[k]-pts[k-1])*(x-ypts[k-1])/(ypts[k]-ypts[k-1])
    res[N-1] = pts[npts-1]

    return res
