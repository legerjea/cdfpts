# Copyright 2018, Université de Technologie de Compiègne, France, 
#                 Jean-Benoist Leger <jbleger@hds.utc.fr>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# vim: syn=pyrex

# cython: language_level=3
# cython: boundscheck=False
# cython: wraparound=False
# cython: cdivision=True

# distutils: language = c++

from libc.math cimport (sqrt, exp, log, INFINITY)
from libcpp.list cimport list as cpplist

cdef Py_ssize_t bisect(double* vect, Py_ssize_t N, double x) nogil:
    cdef Py_ssize_t second = N
    cdef Py_ssize_t first = 0
    cdef Py_ssize_t center = 0
    while first < second:
        center = (first+second)//2
        if vect[center] < x:
            first = center + 1
        else:
            second = center
    return first


cdef double logistic(double x) nogil:
    return 1/(1+exp(-x))


cdef double logit(double x) nogil:
    return log(x)-log(1-x)

cdef double logexpit(double x) nogil:
    if x>=0:
        return -log(1+exp(-x))
    return x-log(1+exp(x))


cdef struct interpolated_t:
    double value
    double slope
    double der2
    unsigned int forced_monotonic


cdef void interpol(interpolated_t* pret, double* xpts, double* lypts, Py_ssize_t N, double x, int localized, Py_ssize_t i, int ensure_monotonic) nogil:
    cdef double dx1,dx2,dy1,dy2,w1,w2,h0,h1,m0,m1
    if not localized:
        i = bisect(xpts, N, x)
    if i==0:
        h0 = xpts[1]-xpts[0]
        h1 = xpts[2]-xpts[1]
        m0 = (lypts[1]-lypts[0])/h0
        m1 = (lypts[2]-lypts[1])/h1
        pret.slope = ((2*h0+h1)*m0-h0*m1)/(h0+h1)
        pret.value = (x-xpts[0])*pret.slope+lypts[0]
        pret.der2 = 0
        return 
    if i==N:
        h0 = xpts[N-2]-xpts[N-1]
        h1 = xpts[N-3]-xpts[N-2]
        m0 = (lypts[N-2]-lypts[N-1])/h0
        m1 = (lypts[N-3]-lypts[N-2])/h1
        pret.slope = ((2*h0+h1)*m0-h0*m1)/(h0+h1)
        pret.value = (x-xpts[i-1])*pret.slope+lypts[i-1]
        pret.der2 = 0
        return 
    cdef Py_ssize_t i2 = i
    cdef Py_ssize_t i1 = i-1
    cdef double slope1internal = 0
    if i1==0:
        h0 = xpts[1]-xpts[0]
        h1 = xpts[2]-xpts[1]
        m0 = (lypts[1]-lypts[0])/h0
        m1 = (lypts[2]-lypts[1])/h1
        slope1internal = ((2*h0+h1)*m0-h0*m1)/(h0+h1)
    else:
        dy1 = lypts[i1+1]-lypts[i1]
        dy2 = lypts[i1]-lypts[i1-1]
        dx1 = xpts[i1+1]-xpts[i1]
        dx2 = xpts[i1]-xpts[i1-1]
        w1 = 2*dx1+dx2
        w2 = dx1+2*dx2
        if dy1*dy2>0:
            slope1internal = (w1+w2)/(w1*dx1/dy1+w2*dx2/dy2)
        else:
            slope1internal = 0
    cdef double slope2internal = 0
    if i2==N-1:
        h0 = xpts[N-2]-xpts[N-1]
        h1 = xpts[N-3]-xpts[N-2]
        m0 = (lypts[N-2]-lypts[N-1])/h0
        m1 = (lypts[N-3]-lypts[N-2])/h1
        slope2internal = ((2*h0+h1)*m0-h0*m1)/(h0+h1)
    else:
        dy1 = lypts[i2+1]-lypts[i2]
        dy2 = lypts[i2]-lypts[i2-1]
        dx1 = xpts[i2+1]-xpts[i2]
        dx2 = xpts[i2]-xpts[i2-1]
        w1 = 2*dx1+dx2
        w2 = dx1+2*dx2
        if dy1*dy2>0:
            slope2internal = (w1+w2)/(w1*dx1/dy1+w2*dx2/dy2)
        else:
            slope2internal = 0
    cdef double dy,factor,sfactor
    cdef double dx = xpts[i2]-xpts[i1]
    slope1internal*=dx
    slope2internal*=dx
    pret.forced_monotonic = 0
    if ensure_monotonic:
        dy = lypts[i2]-lypts[i1]
        if dy>0:
            factor = (slope1internal/dy)**2 + (slope2internal/dy)**2
            if factor>9:
                sfactor = 3/sqrt(factor)
                slope1internal*=sfactor
                slope2internal*=sfactor
                pret.forced_monotonic = 1
    cdef double t = (x-xpts[i1])/(xpts[i2]-xpts[i1])
    pret.value = ( lypts[i1]*(2*t*t*t-3*t*t+1)
            + lypts[i2]*(-2*t*t*t+3*t*t)
            + slope1internal*(t*t*t-2*t*t+t)
            + slope2internal*(t*t*t-t*t)
            )
    pret.slope = ( lypts[i1]*(6*t*t-6*t)
            + lypts[i2]*(6*t-6*t*t)
            + slope1internal*(3*t*t-4*t+1)
            + slope2internal*(3*t*t-2*t)
            ) / dx
    pret.der2 = ( lypts[i1]*(12*t-6)
            + lypts[i2]*(6-12*t)
            + slope1internal*(6*t-4)
            + slope2internal*(6*t-2)
            ) / (dx*dx)
    return


cdef struct cpdp_t:
    double cdf
    double pdf
    double dpdf

cdef struct pdp_t:
    double pdf
    double dpdf

cdef void interp2cpdp(cpdp_t* pret, interpolated_t* interp) nogil:
    cdef double logistic_value = logistic(interp.value)
    cdef double logistic_mvalue = logistic(-interp.value)
    pret.cdf = logistic_value
    pret.pdf = interp.slope * logistic_value * logistic_mvalue
    pret.dpdf = (interp.der2 + interp.slope**2 * (logistic_mvalue-logistic_value)) * logistic_value * logistic_mvalue
    return


cdef double interp2cdf(interpolated_t* interp) nogil:
    return logistic(interp.value)

cdef double interp2logcdf(interpolated_t* interp) nogil:
    return logexpit(interp.value)

cdef double interp2sf(interpolated_t* interp) nogil:
    return logistic(-interp.value)

cdef double interp2logsf(interpolated_t* interp) nogil:
    return logexpit(-interp.value)

cdef double interp2logitcdf(interpolated_t* interp) nogil:
    return interp.value

cdef double interp2pdf(interpolated_t* interp) nogil:
    return (interp.slope * logistic(interp.value) * logistic(-interp.value))

cdef double interp2logpdf(interpolated_t* interp) nogil:
    cdef double logslope
    if interp.slope > 0:
        logslope = log(interp.slope)
    else:
        logslope = - INFINITY
    return (logslope + logexpit(interp.value) + logexpit(-interp.value))

cdef double interp2dpdf(interpolated_t* interp) nogil:
    cdef double logistic_value = logistic(interp.value)
    cdef double logistic_mvalue = logistic(-interp.value)
    return (interp.der2 + interp.slope**2 * (logistic_mvalue-logistic_value)) * logistic_value * logistic_mvalue

cdef struct couple:
    double val
    double der

ctypedef void (*FUN)(couple*, void*, double) nogil


cdef double _integrate(FUN fun, void* materials, double a, double b) nogil:
    cdef couple start, stop
    fun(&start, materials, a)
    fun(&stop, materials, b)
    return _integrate_internal(a, &start, b, &stop, fun, materials, 0)


cdef double _integrate_internal(double a, couple* start, double b, couple* stop,
        FUN fun, void* materials, unsigned int rec_level) nogil:
    if rec_level>=10:
        return (start.val+stop.val)*(b-a)/2
    cdef double dy = stop.val-start.val

    cdef double d1 = (b-a)*start.der-dy
    cdef double d2 = (b-a)*stop.der-dy
    if d1*d1+d2*d2 <= .5*dy*dy or _abs((start.der-stop.der)*(b-a)/12) < .1*_abs((start.val+stop.val)/2):
        return ((start.val+stop.val)/2+(start.der-stop.der)*(b-a)/12)*(b-a)
    cdef double c = (a+b)/2
    cdef couple med
    fun(&med, materials, c)
    return (
             _integrate_internal(a, start, c, &med, fun, materials, rec_level+1)
            +_integrate_internal(c, &med, b, stop, fun, materials, rec_level+1)
           )

cdef struct integration_results_t:
    cpplist[double] xpts
    cpplist[double] ypts

cdef double _integrate_with_results(FUN fun, void* materials, integration_results_t* res, double a, double b, double max_step, double va) nogil:
    cdef couple start, stop
    fun(&start, materials, a)
    fun(&stop, materials, b)
    return _integrate_internal_with_results(va, a, &start, b, &stop, fun, materials, max_step, res, 0)

cdef double _abs(double x) nogil:
    if(x>0):
        return x
    return -x

cdef double _integrate_internal_with_results(double va, double a, couple* start, double b, couple* stop,
        FUN fun, void* materials, double max_step, integration_results_t* res, unsigned int rec_level) nogil:
    cdef double vb
    if rec_level>=10:
        vb = va+ (start.val+stop.val)*(b-a)/2
        res.xpts.push_back(b)
        res.ypts.push_back(vb)
        return vb

    cdef double dy = stop.val-start.val

    cdef double d1 = (b-a)*start.der-dy
    cdef double d2 = (b-a)*stop.der-dy
    if d1*d1+d2*d2 <= .005*dy*dy or _abs((start.der-stop.der)*(b-a)/12) < .001*_abs((start.val+stop.val)/2) and b-a<max_step:
        vb = va + ((start.val+stop.val)/2+(start.der-stop.der)*(b-a)/12)*(b-a)
        res.xpts.push_back(b)
        res.ypts.push_back(vb)
        return vb
    cdef double c = (a+b)/2
    cdef couple med
    fun(&med, materials, c)
    cdef double vc
    vc = _integrate_internal_with_results(va, a, start, c, &med, fun, materials, max_step, res, rec_level+1)
    vb = _integrate_internal_with_results(vc, c, &med, b, stop, fun, materials, max_step, res, rec_level+1)
    return vb
