# Copyright 2018-2020, Université de Technologie de Compiègne, France,
#                      Jean-Benoist Leger <jbleger@hds.utc.fr>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# cython: language_level=3
# cython: boundscheck=False
# cython: wraparound=False
# cython: cdivision=True

# distutils: language = c++

cimport cython
from cpython.ref cimport PyObject
import numpy as np
from libc.math cimport (sqrt, exp)

cdef extern from "math.h":
    float INFINITY

include "common.pxi"

cdef struct intprod_reverseB_materials_t:
    double* xptsA
    double* lyptsA
    Py_ssize_t NA
    double* xptsB
    double* lyptsB
    Py_ssize_t NB
    Py_ssize_t tiA
    Py_ssize_t tiB
    double t

cdef void intprod_reverseB_fun(couple* ret, void* materials, double x) nogil:
    cdef intprod_reverseB_materials_t* params = <intprod_reverseB_materials_t*>materials
    cdef interpolated_t interpA, interpB
    cdef cpdp_t cpdpA, cpdpB

    interpol(&interpA, params.xptsA, params.lyptsA, params.NA, x, 1, params.tiA, 1)
    interpol(&interpB, params.xptsB, params.lyptsB, params.NB, params.t-x, 1, params.tiB, 1)
    interp2cpdp(&cpdpA, &interpA)
    interp2cpdp(&cpdpB, &interpB)

    ret.val = cpdpA.pdf*cpdpB.pdf
    ret.der = cpdpA.dpdf*cpdpB.pdf - cpdpA.pdf*cpdpB.dpdf

cdef double intprod_reverseB(double [:] xptsA, double [:] lyptsA, double [:] xptsB,
        double [:] lyptsB, double t) nogil:
    cdef Py_ssize_t NA = xptsA.shape[0]
    cdef Py_ssize_t NB = xptsB.shape[0]
    cdef double exterior = 5
    cdef double inf = INFINITY

    cdef int iA = -1
    cdef int iB = -1

    cdef double value = 0

    cdef int nextiA, nextiB
    cdef Py_ssize_t tiA,tiB
    cdef double xstart=0, xstop=0, xstartA=0, xstartB=0, xstopA=0, xstopB=0

    cdef intprod_reverseB_materials_t materials
    materials.xptsA = &xptsA[0]
    materials.lyptsA = &lyptsA[0]
    materials.NA = NA
    materials.xptsB = &xptsB[0]
    materials.lyptsB = &lyptsB[0]
    materials.NB = NB

    materials.t = t
    
    while iA<NA+1 or iB<NB+1:

        if iA == -1:
            xstartA,xstopA = -inf,xptsA[0]-exterior*(xptsA[1]-xptsA[0])/(lyptsA[1]-lyptsA[0])
        elif iA == 0:
            xstartA,xstopA = xptsA[0]-exterior*(xptsA[1]-xptsA[0])/(lyptsA[1]-lyptsA[0]),xptsA[0]
        elif iA == NA:
            xstartA,xstopA = xptsA[NA-1],xptsA[NA-1]+exterior*(xptsA[NA-2]-xptsA[NA-1])/(lyptsA[NA-2]-lyptsA[NA-1])
        elif iA == NA+1:
            xstartA,xstopA = xptsA[NA-1]+exterior*(xptsA[NA-2]-xptsA[NA-1])/(lyptsA[NA-2]-lyptsA[NA-1]),inf
        else:
            xstartA,xstopA = xptsA[iA-1],xptsA[iA]

        if iB == -1:
            xstartB,xstopB = -inf,t-xptsB[NB-1]-exterior*(xptsB[NB-2]-xptsB[NB-1])/(lyptsB[NB-2]-lyptsB[NB-1])
        elif iB == 0:
            xstartB,xstopB = t-xptsB[NB-1]-exterior*(xptsB[NB-2]-xptsB[NB-1])/(lyptsB[NB-2]-lyptsB[NB-1]),t-xptsB[NB-1]
        elif iB == NB:
            xstartB,xstopB = t-xptsB[0],t-xptsB[0]+exterior*(xptsB[1]-xptsB[0])/(lyptsB[1]-lyptsB[0])
        elif iB == NB+1:
            xstartB,xstopB = t-xptsB[0]+exterior*(xptsB[0]-xptsB[1])/(lyptsB[0]-lyptsB[1]),inf
        else:
            xstartB,xstopB = t-xptsB[NB-iB],t-xptsB[NB-iB-1]

        if iA == -1 and iB == -1:
            if xstopA<xstopB:
                iA+=1
                continue
            iB+=1
            continue

        if xstartA>xstartB:
            xstart = xstartA
        else:
            xstart = xstartB

        if xstopA<xstopB:
            xstop = xstopA
            nextiA,nextiB = iA+1,iB
        else:
            xstop = xstopB
            nextiA,nextiB = iA,iB+1

        tiA = 0 if iA<=0 else NA if iA>=NA else iA
        tiB = 0 if iB>=NB else NB if iB<=0 else NB-iB

        materials.tiA = tiA
        materials.tiB = tiB

        value += _integrate(&intprod_reverseB_fun, &materials, xstart, xstop)

        iA,iB = nextiA,nextiB

    return value

from cython.parallel import prange

def intprod_reverseB_multiple(double [:] xptsA, double [:] lyptsA, double [:] xptsB,
        double [:] lyptsB, double [:] t):
    cdef double [:] res = np.zeros(t.shape[0])
    cdef Py_ssize_t i

    with nogil:
        for i in prange(t.shape[0]):
            res[i] = intprod_reverseB(xptsA, lyptsA, xptsB, lyptsB, t[i])

    return res


cdef struct integrate_pdf_materials_t:
    double* xpts
    double* pdf
    Py_ssize_t N
    Py_ssize_t i

cdef void integrate_pdf_fun(couple* ret, void* materials, double x) nogil:
    cdef integrate_pdf_materials_t* params = <integrate_pdf_materials_t*>materials
    cdef interpolated_t interp
    interpol(&interp, params.xpts, params.pdf, params.N, x, 1, params.i, 0)
    ret.val = interp.value
    ret.der = interp.slope


def integrate_pdf(double [:] xpts, double [:] pdf):
    cdef double [:] ret = np.zeros(xpts.shape[0])
    cdef Py_ssize_t i
    cdef double accu = 0
    cdef integrate_pdf_materials_t materials
    materials.xpts = &xpts[0]
    materials.pdf = &pdf[0]
    materials.N = xpts.shape[0]

    for i in range(1,xpts.shape[0]):
        materials.i = i
        accu += _integrate(&integrate_pdf_fun, &materials, xpts[i-1], xpts[i])
        ret[i] = accu
    return ret


cdef struct intprod_direct_materials_t:
    double* xptsA
    double* lyptsA
    Py_ssize_t NA
    double* xptsB
    double* lyptsB
    Py_ssize_t NB
    Py_ssize_t tiA
    Py_ssize_t tiB


cdef void intprod_direct_fun(couple* ret, void* materials, double x) nogil:
    cdef intprod_direct_materials_t* params = <intprod_direct_materials_t*>materials
    cdef interpolated_t interpA, interpB
    cdef cpdp_t cpdpA, cpdpB
    interpol(&interpA, params.xptsA, params.lyptsA, params.NA, x, 1, params.tiA, 1)
    interpol(&interpB, params.xptsB, params.lyptsB, params.NB, x, 1, params.tiB, 1)
    interp2cpdp(&cpdpA, &interpA)
    interp2cpdp(&cpdpB, &interpB)
    ret.val = cpdpA.pdf*cpdpB.pdf
    ret.der = cpdpA.pdf*cpdpB.dpdf + cpdpA.dpdf*cpdpB.pdf


def intprod_direct(double [:] xptsA, double [:] lyptsA, double [:] xptsB,
        double [:] lyptsB):
    cdef Py_ssize_t NA = xptsA.shape[0]
    cdef Py_ssize_t NB = xptsB.shape[0]
    cdef double exterior = 5
    cdef double inf = INFINITY

    cdef int iA = -1
    cdef int iB = -1

    cdef double value = 0
    cdef double tmp_value

    cdef int nextiA, nextiB
    cdef Py_ssize_t tiA,tiB
    cdef double xstart=0, xstop=0, xstartA=0, xstartB=0, xstopA=0, xstopB=0

    cdef double [:] retxpts = np.zeros(NA+NB+4)
    cdef double [:] retpypts = np.zeros(NA+NB+4)
    cdef Py_ssize_t retidx = 0

    cdef intprod_direct_materials_t materials
    materials.xptsA = &xptsA[0]
    materials.lyptsA = &lyptsA[0]
    materials.NA = NA
    materials.xptsB = &xptsB[0]
    materials.lyptsB = &lyptsB[0]
    materials.NB = NB

    while iA<NA+1 or iB<NB+1:

        if iA == -1:
            xstartA,xstopA = -inf,xptsA[0]-exterior*(xptsA[1]-xptsA[0])/(lyptsA[1]-lyptsA[0])
        elif iA == 0:
            xstartA,xstopA = xptsA[0]-exterior*(xptsA[1]-xptsA[0])/(lyptsA[1]-lyptsA[0]),xptsA[0]
        elif iA == NA:
            xstartA,xstopA = xptsA[NA-1],xptsA[NA-1]+exterior*(xptsA[NA-2]-xptsA[NA-1])/(lyptsA[NA-2]-lyptsA[NA-1])
        elif iA == NA+1:
            xstartA,xstopA = xptsA[NA-1]+exterior*(xptsA[NA-2]-xptsA[NA-1])/(lyptsA[NA-2]-lyptsA[NA-1]),inf
        else:
            xstartA,xstopA = xptsA[iA-1],xptsA[iA]

        if iB == -1:
            xstartB,xstopB = -inf,xptsB[0]-exterior*(xptsB[1]-xptsB[0])/(lyptsB[1]-lyptsB[0])
        elif iB == 0:
            xstartB,xstopB = xptsB[0]-exterior*(xptsB[1]-xptsB[0])/(lyptsB[1]-lyptsB[0]),xptsB[0]
        elif iB == NB:
            xstartB,xstopB = xptsB[NB-1],xptsB[NB-1]+exterior*(xptsB[NB-2]-xptsB[NB-1])/(lyptsB[NB-2]-lyptsB[NB-1])
        elif iB == NB+1:
            xstartB,xstopB = xptsB[NB-1]+exterior*(xptsB[NB-2]-xptsB[NB-1])/(lyptsB[NB-2]-lyptsB[NB-1]),inf
        else:
            xstartB,xstopB = xptsB[iB-1],xptsB[iB]

        if iA == -1 and iB == -1:
            if xstopA<xstopB:
                retxpts[retidx] = xstopA
                retpypts[retidx] = 0
                retidx+=1
                iA+=1
                continue
            retxpts[retidx] = xstopB
            retpypts[retidx] = 0
            retidx+=1
            iB+=1
            continue

        if xstartA>xstartB:
            xstart = xstartA
        else:
            xstart = xstartB

        if xstopA<xstopB:
            xstop = xstopA
            nextiA,nextiB = iA+1,iB
        else:
            xstop = xstopB
            nextiA,nextiB = iA,iB+1

        tiA = 0 if iA<=0 else NA if iA>=NA else iA
        tiB = 0 if iB<=0 else NB if iB>=NB else iB

        materials.tiA = tiA
        materials.tiB = tiB

        value += _integrate(&intprod_direct_fun, &materials, xstart, xstop)

        assert retidx < retxpts.shape[0]
        retxpts[retidx] = xstop
        retpypts[retidx] = value
        retidx+=1
        iA,iB = nextiA,nextiB

    return (retxpts,retpypts)


cdef struct intprod_direct_likli_materials_t:
    double* xptsA
    double* lyptsA
    Py_ssize_t NA
    PyObject* fun
    Py_ssize_t tiA


cdef void intprod_direct_likli_fun(couple* ret, void* materials, double x) nogil:
    cdef intprod_direct_likli_materials_t* params = <intprod_direct_likli_materials_t*>materials
    cdef interpolated_t interpA
    cdef cpdp_t cpdpA
    cdef double likliB, dlikliB
    interpol(&interpA, params.xptsA, params.lyptsA, params.NA, x, 1, params.tiA, 1)
    interp2cpdp(&cpdpA, &interpA)
    with gil:
        likliB, dlikliB = (<object>params.fun)(x)
    ret.val = cpdpA.pdf * likliB
    ret.der = cpdpA.pdf*dlikliB + cpdpA.dpdf*likliB


def intprod_direct_likli(double [:] xptsA, double [:] lyptsA, likli_fun, double regular_factor):
    cdef Py_ssize_t NA = xptsA.shape[0]
    cdef double exterior = 5
    cdef double inf = INFINITY

    cdef int iA = -1

    cdef double value = 0

    cdef Py_ssize_t tiA
    cdef double xstart=0, xstop=0, xstartA = 0, xstopA = 0

    cdef intprod_direct_likli_materials_t materials
    materials.xptsA = &xptsA[0]
    materials.lyptsA = &lyptsA[0]
    materials.NA = NA
    materials.fun = <PyObject*>likli_fun
    
    cdef integration_results_t res;

    cdef double max_step = (xptsA[NA-1]-xptsA[0])/NA/regular_factor if regular_factor>0 else INFINITY

    while iA<NA+1:

        if iA == -1:
            xstartA,xstopA = -inf,xptsA[0]-exterior*(xptsA[1]-xptsA[0])/(lyptsA[1]-lyptsA[0])
        elif iA == 0:
            xstartA,xstopA = xptsA[0]-exterior*(xptsA[1]-xptsA[0])/(lyptsA[1]-lyptsA[0]),xptsA[0]
        elif iA == NA:
            xstartA,xstopA = xptsA[NA-1],xptsA[NA-1]+exterior*(xptsA[NA-2]-xptsA[NA-1])/(lyptsA[NA-2]-lyptsA[NA-1])
        elif iA == NA+1:
            xstartA,xstopA = xptsA[NA-1]+exterior*(xptsA[NA-2]-xptsA[NA-1])/(lyptsA[NA-2]-lyptsA[NA-1]),inf
        else:
            xstartA,xstopA = xptsA[iA-1],xptsA[iA]

        if iA == -1:
            res.xpts.push_back(xstopA)
            res.ypts.push_back(0)
            iA+=1
            continue

        tiA = 0 if iA<=0 else NA if iA>=NA else iA

        materials.tiA = tiA

        xstart = xstartA
        xstop = xstopA
        value = _integrate_with_results(&intprod_direct_likli_fun, &materials, &res, xstart, xstop, max_step, value)

        iA+=1

    return (res.xpts, res.ypts)


cdef struct intprod_direct_likli_gm_materials_t:
    double* xptsA
    double* lyptsA
    Py_ssize_t NA
    double* gm_K
    double* gm_c
    double* gm_prec
    Py_ssize_t Ngm
    Py_ssize_t tiA


cdef void intprod_direct_likli_gm_fun(couple* ret, void* materials, double x) nogil:
    cdef intprod_direct_likli_gm_materials_t* params = <intprod_direct_likli_gm_materials_t*>materials
    cdef interpolated_t interpA
    cdef cpdp_t cpdpA
    cdef double likliB = 0, dlikliB = 0
    cdef double likli_component
    cdef Py_ssize_t i
    interpol(&interpA, params.xptsA, params.lyptsA, params.NA, x, 1, params.tiA, 1)
    interp2cpdp(&cpdpA, &interpA)
    for i in range(params.Ngm):
        likli_component = params.gm_K[i]* exp(-.5*(x-params.gm_c[i])**2*params.gm_prec[i])
        likliB += likli_component
        dlikliB += -(x-params.gm_c[i])*params.gm_prec[i]*likli_component
    ret.val = cpdpA.pdf * likliB
    ret.der = cpdpA.pdf*dlikliB + cpdpA.dpdf*likliB


def intprod_direct_likli_gm(double [:] xptsA, double [:] lyptsA, double [:] gm_K,
        double [:] gm_c, double [:] gm_prec, double regular_factor):
    cdef Py_ssize_t NA = xptsA.shape[0]
    cdef double exterior = 5
    cdef double inf = INFINITY

    cdef int iA = -1

    cdef double value = 0

    cdef Py_ssize_t tiA
    cdef double xstart=0, xstop=0, xstartA = 0, xstopA = 0

    cdef intprod_direct_likli_gm_materials_t materials
    materials.xptsA = &xptsA[0]
    materials.lyptsA = &lyptsA[0]
    materials.NA = NA
    materials.gm_K = &gm_K[0]
    materials.gm_c = &gm_c[0]
    materials.gm_prec = &gm_prec[0]
    materials.Ngm = gm_K.shape[0]

    cdef integration_results_t res;
    cdef double max_step = (xptsA[NA-1]-xptsA[0])/NA/regular_factor if regular_factor>0 else INFINITY

    while iA<NA+1:

        if iA == -1:
            xstartA,xstopA = -inf,xptsA[0]-exterior*(xptsA[1]-xptsA[0])/(lyptsA[1]-lyptsA[0])
        elif iA == 0:
            xstartA,xstopA = xptsA[0]-exterior*(xptsA[1]-xptsA[0])/(lyptsA[1]-lyptsA[0]),xptsA[0]
        elif iA == NA:
            xstartA,xstopA = xptsA[NA-1],xptsA[NA-1]+exterior*(xptsA[NA-2]-xptsA[NA-1])/(lyptsA[NA-2]-lyptsA[NA-1])
        elif iA == NA+1:
            xstartA,xstopA = xptsA[NA-1]+exterior*(xptsA[NA-2]-xptsA[NA-1])/(lyptsA[NA-2]-lyptsA[NA-1]),inf
        else:
            xstartA,xstopA = xptsA[iA-1],xptsA[iA]

        if iA == -1:
            res.xpts.push_back(xstopA)
            res.ypts.push_back(0)
            iA+=1
            continue

        tiA = 0 if iA<=0 else NA if iA>=NA else iA

        materials.tiA = tiA

        xstart = xstartA
        xstop = xstopA
        value = _integrate_with_results(&intprod_direct_likli_gm_fun, &materials, &res, xstart, xstop, max_step, value)

        iA+=1

    return (res.xpts, res.ypts)
