
Yet Another Approximation Distribution Framework
================================================


Install locally
---------------

```bash
git clone git@gitlab.utc.fr:legerjea/cdfpts
cd cdfpts
pip3 install -e .
```

Development setup
-----------------

You first need to to build and compile the C code from the cython files.

```bash
cd cdfpts
python setup.py build_ext --inplace
```

Run tests
---------

Just run
```bash
cd yadaf
pytest tests
```


Notes
-----

**Works with python ≥ 3.6.**

Python-*legacy* (*a.k.a.* python-2) is *Not* supported.

Installation in Windows 10 with Anaconda
-------------------------------------
Install ```Microsoft Visual Studio Installer``` and install ```Visual Studio Buils Tools``` from it.

In the optional installations select ```Windows 10 SDK Kit``` and ```MSVC v140 - VS 2015 C++ Build Tools```.

Add the path ```C:\Program Files (x86)\Windows Kits\10\bin\10.0.VERSION\x86``` to the ```path``` environment variable.

Copy the files ```rc.exe``` and ```rcdll.dll``` from ```C:\Program Files (x86)\Windows Kits\10\bin\10.0.VERSION\x86``` to 
```C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\bin```.

In Anaconda, create an environment (or use the ```base``` environment, unadvised) and install ```cython```.

Launch an ```Anaconda Powershell Prompt```, move to the ```cdfpts``` directory and run:
```bash
pip install -e .
```